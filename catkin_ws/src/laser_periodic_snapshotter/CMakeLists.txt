cmake_minimum_required(VERSION 3.1.0)
project(laser_periodic_snapshotter)
##############################################################################
# CMake
##############################################################################

# Tell CMake to run moc when necessary:
# set(CMAKE_AUTOMOC ON)

##############################################################################
# Catkin
##############################################################################

# qt_build provides the qt cmake glue, roscpp the comms for a default talker
find_package(catkin REQUIRED COMPONENTS laser_assembler sensor_msgs)

include_directories(include ${catkin_INCLUDE_DIRS})
# Use this to define what the package will export (e.g. libs, headers).
# Since the default here is to produce only a binary, we don't worry about
# exporting anything.
catkin_package(
    INCLUDE_DIRS include
    LIBRARIES ${PROJECT_NAME}
    CATKIN_DEPENDS laser_assembler sensor_msgs
)

add_definitions(-std=c++11)



##############################################################################
# Sections
##############################################################################

file(GLOB_RECURSE LIB_HEADERS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS include/${PROJECT_NAME}/*.hpp include/${PROJECT_NAME}/*.h)

##############################################################################
# Sources
##############################################################################

file(GLOB_RECURSE LIB_SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS src/*.cpp )
# just remove main from library
list(REMOVE_ITEM LIB_SOURCES src/LaserPeriodicSnapshotterNode.cpp)

##############################################################################
# Binaries
##############################################################################

add_library(${PROJECT_NAME} ${LIB_SOURCES} )
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})


add_executable(${PROJECT_NAME}_node src/LaserPeriodicSnapshotterNode.cpp)
target_link_libraries(${PROJECT_NAME}_node ${PROJECT_NAME})

## Install rules

install(TARGETS
  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

################################################
#Add all files in subdirectories of the project in
# a dummy_target so qtcreator have access to all files
FILE(GLOB_RECURSE extra_files
    ${CMAKE_SOURCE_DIR}/*)
add_custom_target(dummy_${PROJECT_NAME} SOURCES ${extra_files})
################################################
