#ifndef LASERPERIODICSNAPSHOTTER_H
#define LASERPERIODICSNAPSHOTTER_H

#include <ros/ros.h>



namespace laser_periodic_snapshotter
{

class LaserPeriodicSnapshotter
{

public:

  LaserPeriodicSnapshotter();

  void timerCallback(const ros::TimerEvent& e);

private:
  ros::NodeHandle mNh;
  ros::NodeHandle mPrivateNh;
  ros::Publisher mPointCloud2Publisher;
  ros::ServiceClient mLaserAssemblerClient;
  ros::Timer timer_;
  bool first_time_;
  double mAssemblePeriod;
  ros::Duration mOverlapTime;
};

}
#endif // LASERPERIODICSNAPSHOTTER_H
