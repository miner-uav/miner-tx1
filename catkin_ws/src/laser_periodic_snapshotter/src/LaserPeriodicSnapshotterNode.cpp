#include "ros/ros.h"
#include "std_msgs/String.h"
#include "laser_periodic_snapshotter/LaserPeriodicSnapshotter.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "laser_periodic_snapshotter");

    laser_periodic_snapshotter::LaserPeriodicSnapshotter snap;
    ros::spin();
    return 0;
}
