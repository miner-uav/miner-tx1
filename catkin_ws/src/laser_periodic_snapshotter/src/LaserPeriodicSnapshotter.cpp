#include "laser_periodic_snapshotter/LaserPeriodicSnapshotter.h"

#include <laser_assembler/AssembleScans2.h>
#include <laser_assembler/AssembleScans2Request.h>
#include <laser_assembler/AssembleScans2Response.h>
#include <sensor_msgs/PointCloud2.h>
namespace laser_periodic_snapshotter
{

LaserPeriodicSnapshotter::LaserPeriodicSnapshotter()
    : mNh()
    , mPrivateNh("~")
    , mAssemblePeriod(1.0)
{

    mPrivateNh.param("assemble_interval_sec", mAssemblePeriod, mAssemblePeriod);
    double overlapTime = 0.0;
    mPrivateNh.param("overlap_time", overlapTime, overlapTime);
    mOverlapTime = ros::Duration(overlapTime);

    // Create a publisher for the clouds that we assemble
    mPointCloud2Publisher = mPrivateNh.advertise<sensor_msgs::PointCloud2>("assembled_cloud2", 1);

    // Create the service client for calling the assembler
    mLaserAssemblerClient = mNh.serviceClient<laser_assembler::AssembleScans2>("assemble_scans2");
    ROS_INFO("mLaserAssemblerClient path: %s", mLaserAssemblerClient.getService().c_str());

    // Start the timer that will trigger the processing loop (timerCallback)
    timer_ = mPrivateNh.createTimer(ros::Duration(mAssemblePeriod), &LaserPeriodicSnapshotter::timerCallback, this);

    // Need to track if we've called the timerCallback at least once
    first_time_ = true;
}

void LaserPeriodicSnapshotter::timerCallback(const ros::TimerEvent &e)
{

    // We don't want to build a cloud the first callback, since we we
    //   don't have a start and end time yet
    if (first_time_)
    {
        first_time_ = false;
        return;
    }

    // Populate our service request based on our timer callback times
    laser_assembler::AssembleScans2 srv;
    srv.request.begin = e.last_real - mOverlapTime;
    srv.request.end   = e.current_real;

    ROS_DEBUG("Assembling from: %f to %f", srv.request.begin.toSec(), srv.request.end.toSec());

    // Make the service call
    if (mLaserAssemblerClient.call(srv))
    {
        //ROS_INFO("Published Cloud with %ux%u points", (uint32_t)(srv.response.cloud.width), (uint32_t)(srv.response.cloud.height));
        mPointCloud2Publisher.publish(srv.response.cloud);
    }
    else
    {
        ROS_ERROR("Error making service call\n");
    }
}
}
