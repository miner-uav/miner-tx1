
#include "rviz_plugin_miner/RvizPanelConfig.h"
#include "rviz_plugin_miner/RvizMinerPannel.h"
#include "rviz_plugin_miner/RvizMinerPannelPlugin.h"
#include <QApplication>

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "rviz_plugin_miner");
    QApplication a(argc, argv);
    // MainWindow w;
    // w.show();

    // rviz_plugin_miner::RvizMinerPannelPlugin p;
    rviz_plugin_miner::RvizMinerPannelPlugin p;
    p.show();
    rviz::Config cfg;
    cfg.mapSetValue(rviz_plugin_miner::KEY_TOPIC_FLIGHT_STATUS, "/flight_status");
    p.load(cfg);
    return a.exec();
}
