#include "rviz_plugin_miner/RvizMinerPannelPlugin.h"
#include "rviz_plugin_miner/RvizPanelConfig.h"
#include <QMenu>
#include <QProcess>
#include <ros/ros.h>

namespace rviz_plugin_miner
{
using namespace boost::property_tree;
void RvizMinerPannelPlugin::updateBatteryIndicator()
{
    mRvizPanel.ui->batteryBar->setRange(2900, 4200);
    mRvizPanel.ui->batteryBar->addRangeColor(mRvizPanel.ui->batteryBar->minimum(), mConfig.getIntValue(KEY_BAT_CELL_MVOLT_ERROR), Qt::red);
    mRvizPanel.ui->batteryBar->addRangeColor(mConfig.getIntValue(KEY_BAT_CELL_MVOLT_ERROR), mConfig.getIntValue(KEY_BAT_CELL_MVOLT_WARN), QColor("#D1CC00"));
    mRvizPanel.ui->batteryBar->addRangeColor(mConfig.getIntValue(KEY_BAT_CELL_MVOLT_WARN), mRvizPanel.ui->batteryBar->maximum(), QColor("#00B050"));
}

RvizMinerPannelPlugin::RvizMinerPannelPlugin(QWidget *parent)
    : rviz::Panel(parent)
    , mConfig()
    , mRosClient(&mConfig)
    , mRvizPanel(this)
    , mHeartBeatTimer(this)
    , mState()
    , mFlightArmed(false)
    , mBatteryVoltageAcc(tag::rolling_window::window_size = 50)
{

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(&mRvizPanel);
    setLayout(layout);

    QIntValidator *cellsValidator = new QIntValidator(0, 16, this);
    mRvizPanel.ui->batteryCellsLineEdit->setValidator(cellsValidator);

    QIntValidator *voltValidator = new QIntValidator(2000, 4200, this);
    mRvizPanel.ui->batteryCellVoltERRORLineEdit->setValidator(voltValidator);
    mRvizPanel.ui->batteryCellVoltWARNLineEdit->setValidator(voltValidator);

    mRvizPanel.ui->batteryBar->setFormat("%1% (%2 [V])");

    // set initial value
    mState.setStatus(WAS_STALE, STATE_COMPONENT_DIAGNOSTIC_SYS);

    mLastBatteryStateTime = mLastDiagnosticTime = mLastFlightStatusTime = ros::Time::now().toSec();
    // heart beat twice a second
    mHeartBeatTimer.setInterval(500);
    mHeartBeatTimer.start();

    mRvizPanel.ui->armStatusButton->setStateNames("ARMED", "DISARMED", "DISARMING...", "ARMING...");
    mRvizPanel.ui->sDKAuthorityStatusButton->setStateNames("GRANTED", "RELEASED", "RELEASING...", "REQUESTING...");

    // connection from config to edit widgets
    connect(&mConfig, &RvizPanelConfig::signalItemChanged, this, [=](const QString &key, const QVariant &value) {
        if (key == KEY_SRV_DJI_ARM)
        {
            mRvizPanel.ui->dJIArmSrvLineEdit->setText(value.toString());
        }
        else if (key == KEY_SRV_SDK_AUTHORITY)
        {
            mRvizPanel.ui->dJISDKAuthotitySrvLineEdit->setText(value.toString());
        }
        else if (key == KEY_SRV_NMPC_EXIT_TELEOP)
        {
            mRvizPanel.ui->nMPCExitTeleopLineEdit->setText(value.toString());
        }
        else if (key == KEY_TOPIC_BATTERY_STATE)
        {
            mRvizPanel.ui->dJIBatteryStateLineEdit->setText(value.toString());
        }
        else if (key == KEY_TOPIC_FLIGHT_STATUS)
        {
            mRvizPanel.ui->dJIFlight_statusTopicLineEdit->setText(value.toString());
        }
        else if (key == KEY_TOPIC_SDK_AUTHORITY_STATUS)
        {
            mRvizPanel.ui->dJISDKAuthorityTopicLineEdit->setText(value.toString());
        }
        else if (key == KEY_UI_LOG_LEVEL)
        {
            mRvizPanel.ui->logLevelComboBox->setCurrentText(value.toString());
        }
        else if (key == KEY_BAT_CELLS)
        {
            mRvizPanel.ui->batteryCellsLineEdit->setText(value.toString());
        }
        else if (key == KEY_BAT_CELL_MVOLT_WARN)
        {
            mRvizPanel.ui->batteryCellVoltWARNLineEdit->setText(value.toString());
        }
        else if (key == KEY_BAT_CELL_MVOLT_ERROR)
        {
            mRvizPanel.ui->batteryCellVoltERRORLineEdit->setText(value.toString());
        }
    });

    connect(&mConfig, &RvizPanelConfig::signalItemCommited, this, [=](const QString &key, const QVariant &value) {
        // Config has changed it need to be saved
        Q_EMIT configChanged();
        if (key == KEY_BAT_CELL_MVOLT_WARN || key == KEY_BAT_CELL_MVOLT_ERROR)
        {
            updateBatteryIndicator();
        }
    });

    connect(&mConfig, &RvizPanelConfig::signalCleanStatus, this, [=](bool clean) {
        // Config has changed it need to be saved
        mRvizPanel.ui->applyButton->setEnabled(!clean);
        mRvizPanel.ui->resetButton->setEnabled(!clean);
    });

    connect(mRvizPanel.ui->dJIArmSrvLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_SRV_DJI_ARM, value);
    });

    connect(mRvizPanel.ui->dJISDKAuthotitySrvLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_SRV_SDK_AUTHORITY, value);
    });

    connect(mRvizPanel.ui->nMPCExitTeleopLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_SRV_NMPC_EXIT_TELEOP, value);
    });

    connect(mRvizPanel.ui->dJIBatteryStateLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_TOPIC_BATTERY_STATE, value);
    });

    connect(mRvizPanel.ui->dJIFlight_statusTopicLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_TOPIC_FLIGHT_STATUS, value);
    });

    connect(mRvizPanel.ui->dJISDKAuthorityTopicLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_TOPIC_SDK_AUTHORITY_STATUS, value);
    });

    connect(mRvizPanel.ui->batteryCellsLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_BAT_CELLS, value);
    });

    connect(mRvizPanel.ui->batteryCellVoltWARNLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_BAT_CELL_MVOLT_WARN, value);
    });

    connect(mRvizPanel.ui->batteryCellVoltERRORLineEdit, &QLineEdit::textChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_BAT_CELL_MVOLT_ERROR, value);
    });

    connect(mRvizPanel.ui->logLevelComboBox, &QComboBox::currentTextChanged, &mConfig, [=](const QString &value) {
        // set
        mConfig.setConfigItem(KEY_UI_LOG_LEVEL, value);
    });

    mConfig.fireItemChangedForAll();

    // apply/reset buttons connections
    connect(mRvizPanel.ui->resetButton, &QPushButton::clicked, this, &RvizMinerPannelPlugin::resetConfig);
    connect(mRvizPanel.ui->applyButton, &QPushButton::clicked, this, &RvizMinerPannelPlugin::applyConfig);

    connect(mRvizPanel.ui->resetDefaultButton, &QPushButton::clicked, this, &RvizMinerPannelPlugin::restoreDefaultConfig);

    connect(mRvizPanel.ui->nMpcExitTeleopButton, &QPushButton::clicked, &mRosClient, &RosClient::exitTeleop);

    connect(mRvizPanel.ui->cancelAlert, &QPushButton::clicked, &mState, &WarningAlertState::cancelSound);

    connect(&mRosClient, &RosClient::signalFlightStatusChanged, &mState, &WarningAlertState::slotFlightStatusChanged);

    // Arm Status Button
    connect(&mRosClient, &RosClient::signalFlightStatusChanged, this, [=](bool armed) {
        mFlightArmed = armed;
        if (armed)
        {
            // we are armed make sure the audio is on and laud
            QProcess::startDetached("amixer -D pulse sset Master 100% unmute > /dev/null");
            mRvizPanel.ui->armStatusButton->slotSetOn();
        }
        else
        {
            mRvizPanel.ui->armStatusButton->slotSetOff();
        }
    });

    connect(mRvizPanel.ui->armStatusButton, &QStatusButton::enteredOffToOn, &mRosClient, &RosClient::drone_arm);
    connect(mRvizPanel.ui->armStatusButton, &QStatusButton::enteredOnToOff, &mRosClient, &RosClient::drone_disarm);

    // SDK Authority Status Button
    connect(&mRosClient, &RosClient::sdkAuthorityGranted, mRvizPanel.ui->sDKAuthorityStatusButton, &QStatusButton::slotSetOn);
    connect(&mRosClient, &RosClient::sdkAuthorityReleased, mRvizPanel.ui->sDKAuthorityStatusButton, &QStatusButton::slotSetOff);
    connect(mRvizPanel.ui->sDKAuthorityStatusButton, &QStatusButton::enteredOffToOn, &mRosClient, &RosClient::requestAuthority);
    connect(mRvizPanel.ui->sDKAuthorityStatusButton, &QStatusButton::enteredOnToOff, &mRosClient, &RosClient::releaseAuthority);

    // Battery state connection
    connect(&mRosClient, &RosClient::batteryState, this, &RvizMinerPannelPlugin::batteryState);
    // diagnostics
    connect(&mRosClient, &RosClient::diagnosticStatus, this, &RvizMinerPannelPlugin::diagnosticStatus);

    // Logs
    connect(&mRosClient, &RosClient::insertLog, this, &RvizMinerPannelPlugin::insertLog);

    connect(mRvizPanel.ui->logLines, &QListWidget::customContextMenuRequested, this, [=](const QPoint &point) {
        QMenu contextMenu(tr("Context menu"), mRvizPanel.ui->logLines);

        QAction action1("Clear", mRvizPanel.ui->logLines);
        connect(&action1, &QAction::triggered, mRvizPanel.ui->logLines, [=]() {
            // remove all items
            mRvizPanel.ui->logLines->clear();
        });
        contextMenu.addAction(&action1);

        contextMenu.exec(mapToGlobal(point));
    });

    // resize name column of diagnostic tree after expansion
    connect(mRvizPanel.ui->diagnosticsTreeWidget, &QTreeWidget::itemExpanded, this, [=]() { mRvizPanel.ui->diagnosticsTreeWidget->resizeColumnToContents(0); });

    connect(&mHeartBeatTimer, &QTimer::timeout, this, &RvizMinerPannelPlugin::slotHeartBeat);

    connect(&mRosClient, &RosClient::flightStatusArrived, this, [=]() { mLastFlightStatusTime = ros::Time::now().toSec(); });

    connect(&mState, &WarningAlertState::stateChanged, this, &RvizMinerPannelPlugin::slotStateChanged);
}

RvizMinerPannelPlugin::~RvizMinerPannelPlugin()
{
}

void RvizMinerPannelPlugin::load(const rviz::Config &config)
{
    rviz::Panel::load(config);
    mConfig.setFromRvizConfig(config, true);
}

void RvizMinerPannelPlugin::save(rviz::Config config) const
{
    rviz::Panel::save(config);
    mConfig.updateRvizConfig(config);
}

void RvizMinerPannelPlugin::resetConfig(bool ttt)
{
    mConfig.resetToDefaults();
}

void RvizMinerPannelPlugin::applyConfig()
{
    mConfig.applyChanges();
}

void RvizMinerPannelPlugin::restoreDefaultConfig()
{
    mConfig.resetToDefaults();
}

void RvizMinerPannelPlugin::batteryState(float miliVolts)
{

    mLastBatteryStateTime = ros::Time::now().toSec();

    mBatteryVoltageAcc(miliVolts);
    float miliVoltsMean   = rolling_mean(mBatteryVoltageAcc);
    float miliVoltPerCell = miliVoltsMean / mConfig.getDoubleValue(KEY_BAT_CELLS);
    mRvizPanel.ui->batteryBar->setValue(miliVoltPerCell);

    // less or equal error value it is bad
    if (miliVoltPerCell <= mConfig.getDoubleValue(KEY_BAT_CELL_MVOLT_ERROR))
    {
        mState.setStatus(WAS_ERROR, STATE_COMPONENT_BATTERY_VALUE);
    }
    // in between its warning
    else if (miliVoltPerCell > mConfig.getDoubleValue(KEY_BAT_CELL_MVOLT_ERROR) && miliVoltPerCell <= mConfig.getDoubleValue(KEY_BAT_CELL_MVOLT_WARN))
    {
        mState.setStatus(WAS_WARNING, STATE_COMPONENT_BATTERY_VALUE);
    }
    // above warning and below 4.3 is considered normal
    else if (miliVoltPerCell > mConfig.getDoubleValue(KEY_BAT_CELL_MVOLT_WARN) && miliVoltPerCell <= 4300)
    {
        mState.setStatus(WAS_NORMAL, STATE_COMPONENT_BATTERY_VALUE);
    }
    // that should leave > 4.3V
    else
    {
        mState.setStatus(WAS_ERROR, STATE_COMPONENT_BATTERY_VALUE);
        ROS_INFO_THROTTLE(0.5, "Abnormal battery voltage: %f [mV] average per cell", miliVoltPerCell);
    }
}

void RvizMinerPannelPlugin::insertLog(uint8_t level, const QString &timestamp, const QString &logLine)
{
    QString line = QString("%1: %2").arg(timestamp, logLine);
    QListWidgetItem *item;
    switch (level)
    {
    case 1: // debug
        item = new QListWidgetItem(QIcon::fromTheme("dialog-information"), line);
        item->setTextColor(Qt::darkGreen);
        break;
    case 2: // info
        item = new QListWidgetItem(QIcon::fromTheme("dialog-information"), line);
        item->setTextColor(Qt::darkGreen);
        break;
    case 4: // WARN
        item = new QListWidgetItem(QIcon::fromTheme("dialog-warning"), line);
        item->setTextColor(Qt::darkYellow);
        break;
    case 8:  // ERROR
    case 16: // FATAL
        item = new QListWidgetItem(QIcon::fromTheme("dialog-error"), line);
        item->setTextColor(Qt::red);
        break;
    default:
        item = new QListWidgetItem(line);
        break;
    }
    item->setToolTip(logLine);

    mRvizPanel.ui->logLines->insertItem(0, item);
    if (mRvizPanel.ui->logLines->count() > 30)
    {
        mRvizPanel.ui->logLines->takeItem(mRvizPanel.ui->logLines->count() - 1);
    }
}

QTreeWidgetItem *RvizMinerPannelPlugin::findDiagnosticItemByFullPath(const QString &fullPath)
{
    QTreeWidgetItemIterator it(mRvizPanel.ui->diagnosticsTreeWidget);
    while (*it)
    {
        QString temp = (*it)->toolTip(0);
        if (temp.compare(fullPath) == 0)
        {
            return *it;
        }
        ++it;
    }
    return NULL;
}

void RvizMinerPannelPlugin::levelStringToColor(const string &currentValue, QColor &bgColor)
{
    // https://www.w3.org/TR/SVG/types.html#ColorKeywords
    if (currentValue.compare("OK") == 0)
    {
        // green
        bgColor.setNamedColor("lawngreen");
    }
    else if (currentValue.compare("Warning") == 0)
    {
        // yellow
        bgColor.setNamedColor("yellow");
    }
    else if (currentValue.compare("Error") == 0 || currentValue.compare("Stale") == 0)
    {
        // red
        bgColor.setNamedColor("red");
    }
    else
    {
        ROS_ERROR("Unknown diagnostic value: %s", currentValue.c_str());
        bgColor.setNamedColor("red");
    }
}

void RvizMinerPannelPlugin::updateDiagnosticTree(QString fullPath, const ptree::path_type &currentPathItem, const ptree &inTree, QTreeWidgetItem *parentTWIPtr)
{
    // root node
    QTreeWidgetItem *currentItem = NULL;
    if (!currentPathItem.empty())
    {
        QString currentValue(inTree.get_value<string>().c_str());
        currentItem = findDiagnosticItemByFullPath(fullPath);
        if (currentItem == NULL)
        {
            if (parentTWIPtr == NULL)
            {
                currentItem = new QTreeWidgetItem(mRvizPanel.ui->diagnosticsTreeWidget);
            }
            else
            {
                currentItem = new QTreeWidgetItem(parentTWIPtr);
            }
            // set the name and resize to match new element
            currentItem->setText(0, QString(currentPathItem.dump().c_str()));
            currentItem->setToolTip(0, fullPath);
            mRvizPanel.ui->diagnosticsTreeWidget->expandToDepth(1);
            mRvizPanel.ui->diagnosticsTreeWidget->sortItems(0, Qt::SortOrder::AscendingOrder);
        }
        // set value and color
        currentItem->setText(1, currentValue);
        QColor bgColor;
        levelStringToColor(currentValue.toStdString(), bgColor);
        currentItem->setBackgroundColor(1, bgColor);
    }

    for (auto &node : inTree)
    {
        if (fullPath.isEmpty())
        {
            updateDiagnosticTree(QString(node.first.c_str()), node.first, node.second, currentItem);
        }
        else
        {
            updateDiagnosticTree(fullPath.append("/").append(node.first.c_str()), node.first, node.second, currentItem);
        }
    }
}

void RvizMinerPannelPlugin::diagnosticStatus(int8_t lvlv, boost::shared_ptr<ptree> diagTreePtr)
{
    mLastDiagnosticTime = ros::Time::now().toSec();

    mState.setStatus(static_cast<WarningAlertStates>(lvlv), STATE_COMPONENT_DIAGNOSTIC_VAL);

    for (auto &node : *diagTreePtr)
    {
        updateDiagnosticTree("", node.first, node.second, NULL);
    }
}

void RvizMinerPannelPlugin::slotHeartBeat()
{
    static uint32_t hbCount = 0;
    if (mFlightArmed && (hbCount % 10u == 0))
    {

        QProcess::startDetached("amixer -D pulse sset Master 100% unmute > /dev/null");
    }
    hbCount++;

    // lets tolerate 1 sec before rising alarm
    double threshold   = 1.0;
    double currentTime = ros::Time::now().toSec();
    if (fabs(currentTime - mLastBatteryStateTime) > threshold)
    {
        mState.setStatus(WAS_STALE, STATE_COMPONENT_BATTERY_STATUS);
    }
    else
    {
        mState.setStatus(WAS_NORMAL, STATE_COMPONENT_BATTERY_STATUS);
    }

    if (fabs(currentTime - mLastDiagnosticTime) > threshold)
    {
        mState.setStatus(WAS_STALE, STATE_COMPONENT_DIAGNOSTIC_SYS);
    }
    else
    {
        mState.setStatus(WAS_NORMAL, STATE_COMPONENT_DIAGNOSTIC_SYS);
    }

    if (fabs(currentTime - mLastFlightStatusTime) > threshold)
    {
        mState.setStatus(WAS_STALE, STATE_COMPONENT_FLIGHT_ST);
    }
    else
    {
        mState.setStatus(WAS_NORMAL, STATE_COMPONENT_FLIGHT_ST);
    }
}

void RvizMinerPannelPlugin::slotStateChanged(WarningAlertStates state, const string &message)
{
    QColor bg;
    switch (state)
    {
    case WAS_NORMAL:
        bg.setNamedColor("lawngreen");
        break;
    case WAS_WARNING:
        bg.setNamedColor("yellow");
        break;
    case WAS_ERROR:
    case WAS_STALE:
        bg.setNamedColor("red");
        break;
    default:
        break;
    }
    mRvizPanel.ui->systemStatusLineEdit->setText(message.c_str());
    QPalette palette(mRvizPanel.ui->systemStatusLineEdit->palette());
    palette.setColor(QPalette::Base, bg);
    palette.setColor(QPalette::Text, Qt::black);
    mRvizPanel.ui->systemStatusLineEdit->setPalette(palette);
}

} // namespace rviz_plugin_miner

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_plugin_miner::RvizMinerPannelPlugin, rviz::Panel)
