#include "rviz_plugin_miner/QStatusButton.h"

QStatusButton::QStatusButton(QWidget *parent)
    : QPushButton(parent)
    , stateNameOff("Off")
    , stateNameOn("On")
    , stateNameOnToOff("Switching Off")
    , stateNameOffToOn("Switching On")
    , styleOn("background-color: rgb(0, 255, 0);"
              "border: 1px solid black;"
              "border-radius: 5px;")
    , styleOff("background-color: rgb(255, 0, 0);"
               "border: 1px solid black;"
               "border-radius: 5px;")
    , flashingTimer(this)
    , flashingInterval_(250)
{
    flashingTimer.setInterval(flashingInterval_);
    setupFsmStates();
    //    QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
    //    sizePolicy1.setHorizontalStretch(0);
    //    sizePolicy1.setVerticalStretch(0);
    //    sizePolicy1.setHeightForWidth(sizePolicy().hasHeightForWidth());
    //    setSizePolicy(sizePolicy1);
    //    setMinimumSize(QSize(20, 20));
    //    setMaximumSize(QSize(80, 30));
}

QStatusButton::~QStatusButton()
{
}

void QStatusButton::setupFsmStates()
{
    stateOff.setObjectName("stateOff");
    stateOn.setObjectName("stateOn");

    stateOff.assignProperty(this, "styleSheet", styleOff);
    stateOn.assignProperty(this, "styleSheet", styleOn);

    setStateNames("On", "Off", "On...Off", "Off...On");

    // Button clicks
    stateOn.addTransition(this, SIGNAL(clicked()), &stateOnToOff);
    stateOff.addTransition(this, SIGNAL(clicked()), &stateOffToOn);

    // State set transitions
    stateOff.addTransition(this, SIGNAL(signalSetOn()), &stateOn);

    stateOn.addTransition(this, SIGNAL(signalSetOff()), &stateOff);

    stateOnToOff.addTransition(this, SIGNAL(signalSetOn()), &stateOn);
    stateOnToOff.addTransition(this, SIGNAL(signalSetOff()), &stateOff);

    stateOffToOn.addTransition(this, SIGNAL(signalSetOn()), &stateOn);
    stateOffToOn.addTransition(this, SIGNAL(signalSetOff()), &stateOff);

    // those transition are for debugging only
    // stateOnToOff.addTransition(this, SIGNAL(clicked()), stateOff);
    // stateOffToOn.addTransition(this, SIGNAL(clicked()), stateOn);

    // UI Flashing transitions
    connect(&stateOnToOff, SIGNAL(entered()), &flashingTimer, SLOT(start()));
    connect(&stateOffToOn, SIGNAL(entered()), &flashingTimer, SLOT(start()));
    connect(&stateOnToOff, SIGNAL(exited()), &flashingTimer, SLOT(stop()));
    connect(&stateOffToOn, SIGNAL(exited()), &flashingTimer, SLOT(stop()));

    // external notifications
    connect(&stateOff, SIGNAL(entered()), this, SLOT(onEnteredOff()));
    connect(&stateOn, SIGNAL(entered()), this, SLOT(onEnteredOn()));
    connect(&stateOffToOn, SIGNAL(entered()), this, SLOT(onEnteredOffToOn()));
    connect(&stateOnToOff, SIGNAL(entered()), this, SLOT(onEnteredOnToOff()));

    machine.addState(&stateOff);
    machine.addState(&stateOn);
    machine.addState(&stateOffToOn);
    machine.addState(&stateOnToOff);
    machine.setInitialState(&stateOff);
    machine.start();

    connect(&flashingTimer, &QTimer::timeout, this, &QStatusButton::toggleStyle);
}

void QStatusButton::slotSetOn()
{
    Q_EMIT signalSetOn();
}

void QStatusButton::slotSetOff()
{
    Q_EMIT signalSetOff();
}

void QStatusButton::setStateNames(QString on, QString off, QString onToOff, QString offToOn)
{
    stateNameOn      = on;
    stateNameOff     = off;
    stateNameOnToOff = onToOff;
    stateNameOffToOn = offToOn;
    stateOn.assignProperty(this, "text", stateNameOn);
    stateOff.assignProperty(this, "text", stateNameOff);
    stateOnToOff.assignProperty(this, "text", stateNameOnToOff);
    stateOffToOn.assignProperty(this, "text", stateNameOffToOn);
}

void QStatusButton::toggleStyle()
{
    if (currentStyle == &styleOn)
    {
        currentStyle = &styleOff;
        setStyleSheet(styleOff);
    }
    else
    {
        currentStyle = &styleOn;
        setStyleSheet(styleOn);
    }
}

void QStatusButton::onEnteredOn()
{
    Q_EMIT enteredOn();
}

void QStatusButton::onEnteredOff()
{
    Q_EMIT enteredOff();
}

void QStatusButton::onEnteredOnToOff()
{
    Q_EMIT enteredOnToOff();
}

void QStatusButton::onEnteredOffToOn()
{
    Q_EMIT enteredOffToOn();
}
