#include "rviz_plugin_miner/RvizMinerPannel.h"
#include "ui_rvizminerpannel.h"

namespace rviz_plugin_miner
{

RvizMinerPannel::RvizMinerPannel(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::RvizMinerPannel)
{
    ui->setupUi(this);
    ui->armStatusButton->setStateNames("ARMED", "DISARMED", "DISARMING...", "ARMING...");
}

RvizMinerPannel::~RvizMinerPannel()
{
    delete ui;
}

} // namespace
