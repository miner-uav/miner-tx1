#include "rviz_plugin_miner/RosClient.h"
#include "rviz_plugin_miner/RvizPanelConfig.h"
#include <boost/algorithm/string.hpp>
#include <dji_sdk/DroneArmControl.h>
#include <dji_sdk/SDKControlAuthority.h>
#include <iostream>
#include <std_srvs/Empty.h>

namespace rviz_plugin_miner
{

using namespace boost::property_tree;
RosClient::RosClient(RvizPanelConfig *config)
    : nh_()
    , mConfig(config)
    , mLogLevelFlag(2)
{

    mSubRosLog = nh_.subscribe<rosgraph_msgs::Log>("/rosout", 10, &RosClient::cb_ros_log, this);
    mSubDiag   = nh_.subscribe<diagnostic_msgs::DiagnosticStatus>("/diagnostics_toplevel_state", 10, &RosClient::cb_diagnostics, this);

    connect(mConfig, &RvizPanelConfig::signalItemCommited, this, [=](const QString &key, const QVariant &value) {
        string sValue = value.toString().toStdString();
        if (key == KEY_SRV_DJI_ARM)
        {
            mSrvDroneArmClient.shutdown();
            mSrvDroneArmClient = nh_.serviceClient<dji_sdk::DroneArmControl>(sValue);
        }
        else if (key == KEY_SRV_SDK_AUTHORITY)
        {
            mSrvSdkAuthorityClient.shutdown();
            mSrvSdkAuthorityClient = nh_.serviceClient<dji_sdk::SDKControlAuthority>(sValue);
        }
        else if (key == KEY_SRV_NMPC_EXIT_TELEOP)
        {
            mSrvNmpcExitTeleopClient.shutdown();
            mSrvNmpcExitTeleopClient = nh_.serviceClient<std_srvs::Empty>(sValue);
        }
        else if (key == KEY_TOPIC_BATTERY_STATE)
        {
            mSubBatteryState.shutdown();
            mSubBatteryState = nh_.subscribe<sensor_msgs::BatteryState>(sValue, 3, &RosClient::cb_dji_battery_state, this);
        }
        else if (key == KEY_TOPIC_FLIGHT_STATUS)
        {
            mSubFlightStatus.shutdown();
            mSubFlightStatus = nh_.subscribe<std_msgs::UInt8>(sValue, 3, &RosClient::cb_dji_flight_status, this);
        }
        else if (key == KEY_TOPIC_SDK_AUTHORITY_STATUS)
        {
            mSubSdkAuthorityStatus.shutdown();
            mSubSdkAuthorityStatus = nh_.subscribe<std_msgs::UInt8>(sValue, 3, &RosClient::cb_sdk_authority_status, this);
        }
        else if (key == KEY_UI_LOG_LEVEL)
        {
            updateLogLevel(sValue);
        }
    });
}

RosClient::~RosClient()
{
}

void RosClient::drone_arm()
{
    dji_sdk::DroneArmControlRequest request;
    dji_sdk::DroneArmControlResponse response;
    request.arm = 1;
    if (mSrvDroneArmClient.call(request, response))
    {
        if (response.result)
        {
            ROS_INFO("ARMING DRONE - response POSITIVE");
        }
        else
        {
            ROS_INFO("ARMING DRONE - response NEGATIVE");
        }
    }
    else
    {
        ROS_INFO("ARMING DRONE - call failed");
    }
}

void RosClient::drone_disarm()
{
    dji_sdk::DroneArmControlRequest request;
    dji_sdk::DroneArmControlResponse response;
    request.arm = 0;
    if (mSrvDroneArmClient.call(request, response))
    {
        if (response.result)
        {
            ROS_INFO("DISARMING DRONE - response POSITIVE");
        }
        else
        {
            ROS_INFO("DISARMING DRONE - response NEGATIVE");
        }
    }
    else
    {
        ROS_INFO("DISARMING DRONE - call failed");
    }
}

void RosClient::requestAuthority()
{
    dji_sdk::SDKControlAuthorityRequest req;
    dji_sdk::SDKControlAuthorityResponse res;
    req.control_enable = dji_sdk::SDKControlAuthorityRequest::REQUEST_CONTROL;
    bool call_status   = mSrvSdkAuthorityClient.call(req, res);

    if (call_status)
    {
        if (res.result)
        {
            Q_EMIT sdkAuthorityGranted();
            ROS_INFO("SDK Authority Granted");
        }
        else
        {
            if (res.ack_data == 2)
            {
                ROS_WARN("SDK Authority managed by RC controled");
            }
            ROS_ERROR("SDK Authority request refused");
        }
    }
    else
    {
        ROS_ERROR("SDK Authority call failed");
    }
}

void RosClient::releaseAuthority()
{
    dji_sdk::SDKControlAuthorityRequest req;
    dji_sdk::SDKControlAuthorityResponse res;
    req.control_enable = dji_sdk::SDKControlAuthorityRequest::RELEASE_CONTROL;
    bool call_status   = mSrvSdkAuthorityClient.call(req, res);
    if (call_status)
    {
        if (res.result)
        {
            Q_EMIT sdkAuthorityReleased();
            ROS_INFO("SDK Authority Released");
        }
        else
        {
            if (res.ack_data == 2)
            {
                ROS_WARN("SDK Authority managed by RC controled");
            }
            ROS_ERROR("SDK Authority relese refused");
        }
    }
    else
    {
        ROS_ERROR("SDK Authority call failed");
    }
}

void RosClient::exitTeleop()
{
    std_srvs::EmptyRequest req;
    std_srvs::EmptyResponse res;
    bool call_status = mSrvNmpcExitTeleopClient.call(req, res);
    if (call_status)
    {
        ROS_INFO("Exit Teleop Requested and accepted");
    }
    else
    {
        ROS_ERROR("Exit Teleop call failed");
    }
}

void RosClient::cb_dji_flight_status(const std_msgs::UInt8ConstPtr &statusPtr)
{
    static uint32_t count;
    count++;
    if (statusPtr->data == 0) // ARMED
    {
        if (drone_armed_ || count % 10 == 0)
        {
            if (drone_armed_ == true)
            {
                ROS_INFO("UAV DISARMED");
            }
            drone_armed_ = false;
            Q_EMIT signalFlightStatusChanged(drone_armed_);
        }
    }
    else // DISARMED
    {
        if (!drone_armed_ || count % 10 == 0)
        {
            if (drone_armed_ == false)
            {
                ROS_INFO("UAV ARMED");
            }
            drone_armed_ = true;
            Q_EMIT signalFlightStatusChanged(drone_armed_);
        }
    }
    Q_EMIT flightStatusArrived();
}

void RosClient::cb_dji_battery_state(const sensor_msgs::BatteryStateConstPtr &statePtr)
{
    Q_EMIT batteryState(statePtr->voltage);
}

void RosClient::cb_sdk_authority_status(const std_msgs::UInt8ConstPtr &statusPtr)
{
    static uint32_t count;
    count++;
    if (statusPtr->data == 0) // ARMED
    {
        if (sdk_authority_granted_ || count % 10 == 0)
        {
            sdk_authority_granted_ = false;
            Q_EMIT sdkAuthorityReleased();
        }
    }
    else // DISARMED
    {
        if (!sdk_authority_granted_ || count % 10 == 0)
        {
            sdk_authority_granted_ = true;
            Q_EMIT sdkAuthorityGranted();
        }
    }
}

void RosClient::cb_ros_log(const rosgraph_msgs::LogConstPtr &logMsgPtr)
{
    // ignote gdb errors
    if (boost::starts_with(logMsgPtr->msg, "poll failed"))
    {
        return;
    }

    if (logMsgPtr->level < mLogLevelFlag)
    {
        return;
    }
    QString stamp   = FormatTime(logMsgPtr->header.stamp);
    QString message = QString(logMsgPtr->msg.c_str());
    Q_EMIT insertLog(logMsgPtr->level, stamp, message);
}

void RosClient::cb_diagnostics(const diagnostic_msgs::DiagnosticStatusConstPtr &diagMsgPtr)
{

    boost::shared_ptr<ptree> diagnosticsElementsPtr = boost::make_shared<ptree>("MINER");

    for (int valueIdx = 0; valueIdx < diagMsgPtr->values.size(); valueIdx++) // n_pairs is the number of KeyValue pairs for each diagnosticStatus
    {
        string key   = diagMsgPtr->values[valueIdx].key;
        string value = diagMsgPtr->values[valueIdx].value;
        diagnosticsElementsPtr->put(ptree::path_type(key, '/'), value);
    }
    Q_EMIT diagnosticStatus(diagMsgPtr->level, diagnosticsElementsPtr);
}

void RosClient::updateLogLevel(const std::string &levelName)
{
    if (levelName.compare("DEBUG") == 0)
    {
        mLogLevelFlag = 1;
    }
    else if (levelName.compare("INFO") == 0)
    {
        mLogLevelFlag = 2;
    }
    else if (levelName.compare("WARN") == 0)
    {
        mLogLevelFlag = 4;
    }
    else if (levelName.compare("ERROR") == 0)
    {
        mLogLevelFlag = 8;
    }
    else if (levelName.compare("FATAL") == 0)
    {
        mLogLevelFlag = 16;
    }
    else
    {
        ROS_ERROR("Invalid log level name: %s", levelName.c_str());
    }
}

QString RosClient::FormatTime(const ros::Time &date_time)
{

    boost::posix_time::time_facet *facet = new boost::posix_time::time_facet();
    facet->format("%Y-%b-%d %H:%M:%S%F %z");
    std::stringstream ss;
    ss.imbue(std::locale(std::wcout.getloc(), facet));
    ss << date_time.toBoost();

    return QString(ss.str().c_str());
}

} // namespace rviz_plugin_miner
