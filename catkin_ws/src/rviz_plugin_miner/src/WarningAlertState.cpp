#include "rviz_plugin_miner/WarningAlertState.h"

#include <QMapIterator>
#include <boost/algorithm/string/join.hpp>

namespace rviz_plugin_miner
{
using namespace std;

WarningAlertState::WarningAlertState()
    : mCurrentState(WAS_NORMAL)
    , mSoundRedAlert(this)
    , mPlaySound(false)
    , mFlightArmed(false)

{
    mSoundRedAlert.setSource(QUrl::fromLocalFile(":/rviz_plugin_miner/RedAlert.wav"));

    connect(&mSoundRedAlert, &QSoundEffect::playingChanged, this, [=]() {
        if (mPlaySound && !mSoundRedAlert.isPlaying())
        {
            mSoundRedAlert.play();
        }
    });
}

WarningAlertState::~WarningAlertState()
{
}

void WarningAlertState::setStatus(WarningAlertStates state, const QString &subsystem)
{
    boost::mutex::scoped_lock lock(mMutex);
    mComponentStatus[subsystem] = state;
    evaluateState();
}

void WarningAlertState::slotFlightStatusChanged(bool armed)
{
    if (mFlightArmed != armed)
    {
        mFlightArmed = armed;

        if (mFlightArmed && mCurrentState >= WAS_ERROR)
        {
            mPlaySound = true;
            mSoundRedAlert.play();
        }
        else
        {
            mPlaySound = false;
            mSoundRedAlert.stop();
        }
    }
}

void WarningAlertState::cancelSound()
{
    mPlaySound = false;
    mSoundRedAlert.stop();
}

string WarningAlertState::levelToString(WarningAlertStates level)
{
    switch (mCurrentState)
    {
    case WAS_ERROR:
        return "ERROR";
    case WAS_WARNING:
        return "WARNING";
    case WAS_STALE:
        return "STALE";
    case WAS_NORMAL:
        return "OK";
    default:
        return "UNKNOWN";
    }
}
void WarningAlertState::evaluateState()
{
    vector<string> errorMsgs;
    WarningAlertStates newState = WAS_NORMAL;

    QMapIterator<QString, WarningAlertStates> compStateItt(mComponentStatus);
    while (compStateItt.hasNext())
    {
        compStateItt.next();

        if (compStateItt.value() > newState)
        {
            errorMsgs.push_back(levelToString(compStateItt.value()) + ": " + compStateItt.key().toStdString());
            newState = compStateItt.value();
        }
    }

    sort(errorMsgs.begin(), errorMsgs.end());

    mCurrentState = newState;
    switch (mCurrentState)
    {
    case WAS_ERROR:
    case WAS_STALE:
    {
        if (mFlightArmed)
        {
            mPlaySound = true;
            mSoundRedAlert.play();
        }
        string message = boost::algorithm::join(errorMsgs, ", ");
        Q_EMIT stateChanged(mCurrentState, message);
    }
    break;
    case WAS_WARNING:
    {
        string message = boost::algorithm::join(errorMsgs, ", ");
        Q_EMIT stateChanged(mCurrentState, message);
    }
    break;
    case WAS_NORMAL:
        if (mSoundRedAlert.isPlaying())
        {
            mPlaySound = false;
            mSoundRedAlert.stop();
        }

        Q_EMIT stateChanged(mCurrentState, "OK");
        break;
    default:
        break;
    }
}

} // namespace rviz_plugin_miner
