#include "rviz_plugin_miner/RvizPanelConfig.h"

namespace rviz_plugin_miner
{

RvizPanelConfig::RvizPanelConfig()
    : lastCleanValue(true)
{
    mConfigItems[KEY_SRV_DJI_ARM]                = new ConfigItem("/dji_sdk/drone_arm_control");
    mConfigItems[KEY_SRV_SDK_AUTHORITY]          = new ConfigItem("/dji_sdk/sdk_control_authority");
    mConfigItems[KEY_SRV_NMPC_EXIT_TELEOP]       = new ConfigItem("/miner/back_to_position_hold");
    mConfigItems[KEY_TOPIC_FLIGHT_STATUS]        = new ConfigItem("/dji_sdk/flight_status");
    mConfigItems[KEY_TOPIC_BATTERY_STATE]        = new ConfigItem("/dji_sdk/battery_state");
    mConfigItems[KEY_TOPIC_SDK_AUTHORITY_STATUS] = new ConfigItem("/dji_sdk/sdk_authotity_status");
    mConfigItems[KEY_UI_LOG_LEVEL]               = new ConfigItem("INFO");
    mConfigItems[KEY_BAT_CELLS]                  = new ConfigItem(4);
    mConfigItems[KEY_BAT_CELL_MVOLT_WARN]         = new ConfigItem(3500.0);
    mConfigItems[KEY_BAT_CELL_MVOLT_ERROR]        = new ConfigItem(3000.0);
}
/**
 * @brief RvizPanelConfig::setFromRvizConfig
 * @param config
 * @param forceChangeNotification
 * @return true if missing items are detected and config need saving
 */
void RvizPanelConfig::setFromRvizConfig(const rviz::Config &config, bool forceChangeNotification)
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        QVariant temp;
        if (config.mapGetValue(itt.key(), &temp))
        {
            // if value existe then we us ie
            if (mConfigItems[itt.key()]->setWorkingValue(temp))
            {
                Q_EMIT signalItemChanged(itt.key(), itt.value()->getWorkingValue());
            }
        }
        else
        {
            // else we use default
            if (mConfigItems[itt.key()]->setWorkingToDefaultValue())
            {
                Q_EMIT signalItemChanged(itt.key(), itt.value()->getWorkingValue());
            }
        }
        bool applyChange = mConfigItems[itt.key()]->ApplyChange();
        if (applyChange || forceChangeNotification)
        {
            Q_EMIT signalItemCommited(itt.key(), itt.value()->getActiveValue());
        }
    }
    checkClean();
}

void RvizPanelConfig::fireItemChangedForAll()
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        Q_EMIT signalItemChanged(itt.key(), itt.value()->getWorkingValue());

    }
}

void RvizPanelConfig::updateRvizConfig(rviz::Config &config) const
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        config.mapSetValue(itt.key(), itt.value()->getActiveValue());
    }
}

void RvizPanelConfig::setConfigItem(QString key, const QVariant &value)
{
    assert(mConfigItems.contains(key));
    if (mConfigItems[key]->setWorkingValue(value))
    {
        Q_EMIT signalItemChanged(key, value);
    }
    checkClean();
}

double RvizPanelConfig::getDoubleValue(QString key)
{
    assert(mConfigItems.contains(key));
    return mConfigItems[key]->getActiveValue().toDouble();
}

int RvizPanelConfig::getIntValue(QString key)
{
    assert(mConfigItems.contains(key));
    return mConfigItems[key]->getActiveValue().toInt();
}

QString RvizPanelConfig::getStringValue(QString key)
{
    assert(mConfigItems.contains(key));
    return mConfigItems[key]->getActiveValue().toString();
}

void RvizPanelConfig::resetToDefaults()
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        if (mConfigItems[itt.key()]->setWorkingToDefaultValue())
        {
            Q_EMIT signalItemChanged(itt.key(), itt.value()->getWorkingValue());
        }
    }
    checkClean();
}

void RvizPanelConfig::resetChanges()
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        if (mConfigItems[itt.key()]->resetWorkingValueChanges())
        {
            Q_EMIT signalItemChanged(itt.key(), itt.value()->getWorkingValue());
        }
    }
    checkClean();
}

void RvizPanelConfig::applyChanges()
{
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        if (mConfigItems[itt.key()]->ApplyChange())
        {
            Q_EMIT signalItemCommited(itt.key(), itt.value()->getActiveValue());
        }
    }
    checkClean();
}

void RvizPanelConfig::checkClean()
{
    bool clean = true;
    QMapIterator<QString, ConfigItem *> itt(mConfigItems);
    while (itt.hasNext())
    {
        itt.next();
        if (!itt.value()->isClean())
        {
            clean = false;
        }
    }
    if (lastCleanValue != clean)
    {
        lastCleanValue = clean;
        Q_EMIT signalCleanStatus(lastCleanValue);
    }
}
}
