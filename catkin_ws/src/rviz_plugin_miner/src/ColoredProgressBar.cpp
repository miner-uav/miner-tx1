#include "rviz_plugin_miner/ColoredProgressBar.h"

#include <QBrush>
#include <QColor>
#include <QPainter>
#include <QPen>
#include <QStyle>

using namespace std;

ColoredProgressBar::ColoredProgressBar(QWidget *parent)
    : QProgressBar(parent)
    , mMarginX(1)
    , mMarginY(1)
    , mPercentOffset(-500.0)
    , mPercentScale(0.1428571)
{
}

ColoredProgressBar::~ColoredProgressBar()
{
}

/**
 * @brief ColoredProgressBar::addRangeColor ranges has to be added in order
 * @param min_incl - inclusive minimum
 * @param max_excl - exclusive maximum
 * @param color - text color code #RRGGBB
 */
void ColoredProgressBar::addRangeColor(int min_incl, int max_excl, const QColor &color)
{
    // we leaving 1 pixel border
    mRanges.push_back(range_t(min_incl, max_excl, color));
}

void ColoredProgressBar::resetColorRanges()
{
    mRanges.clear();
    mRangesPixels.clear();
}

void ColoredProgressBar::paintEvent(QPaintEvent *)
{
    recalculateRangePixels();
    int val = value();
    int pos = QStyle::sliderPositionFromValue(minimum(), maximum(), val, width() - 2 * mMarginX);

    int bar_with   = 0;
    int bar_height = height() - 2 * mMarginY;

    QPainter p(this);
    for (ranges_t::const_iterator i = mRangesPixels.begin(); i != mRangesPixels.end(); ++i)
    {
        int min             = get<0>(*i);
        int max             = get<1>(*i);
        const QColor &color = get<2>(*i);
        // do we need to draw it
        if (min < pos)
        {
            p.setPen(color);
            p.setBrush(color);

            // draw full rect
            if (pos > max)
            {
                bar_with = max - min;
            }
            else // draw partial bar
            {
                bar_with = pos - min;
            }
            p.drawRect(min + mMarginX, mMarginY, bar_with, bar_height);
        }
    }

    p.setPen(Qt::lightGray);
    p.setBrush(QBrush(Qt::lightGray));
    p.drawRect(pos + mMarginX, mMarginY, (width() - 2 * mMarginX) - pos, bar_height);

    p.setPen(Qt::black);
    p.setBrush(QBrush(Qt::black));
    float percent = val * mPercentScale + mPercentOffset;
    QString qtext = QString(format()).arg(QString::number(percent, 'f', 1), QString::number(val / 1000.0, 'f', 2));
    p.drawText(0, 0, width(), height(), Qt::AlignCenter, qtext);
}

void ColoredProgressBar::recalculateRangePixels()
{
    mRangesPixels.clear();
    for (ranges_t::const_iterator i = mRanges.begin(); i != mRanges.end(); ++i)
    {
        int px_min = QStyle::sliderPositionFromValue(minimum(), maximum(), get<0>(*i), width() - 2 * mMarginX);
        int px_max = QStyle::sliderPositionFromValue(minimum(), maximum(), get<1>(*i), width() - 2 * mMarginX);
        mRangesPixels.push_back(range_t(px_min, px_max, get<2>(*i)));
    }
}
