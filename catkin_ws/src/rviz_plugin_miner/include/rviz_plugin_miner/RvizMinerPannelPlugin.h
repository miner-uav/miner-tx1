#pragma once

#include "rviz_plugin_miner/RosClient.h"
#include "rviz_plugin_miner/RvizMinerPannel.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#include "WarningAlertState.h"
#include "ui_rvizminerpannel.h"
#include <QtMultimedia/QSound>
#include <rosgraph_msgs/Log.h>
#include <rviz/panel.h>

namespace rviz_plugin_miner
{
using namespace boost::property_tree;
using namespace boost::accumulators;
class RvizMinerPannelPlugin : public rviz::Panel
{
    // This class uses Qt slots and is a subclass of QObject, so it needs
    // the Q_OBJECT macro.
    Q_OBJECT
  public:
    RvizMinerPannelPlugin(QWidget *parent = 0);
    ~RvizMinerPannelPlugin();

    // Panel interface
  public:
    void load(const rviz::Config &config) override;
    void save(rviz::Config config) const override;

  protected:
    RvizPanelConfig mConfig;
    RosClient mRosClient;
    RvizMinerPannel mRvizPanel;
    QTimer mHeartBeatTimer;
    double mLastFlightStatusTime;
    double mLastBatteryStateTime;
    double mLastDiagnosticTime;
    bool mFlightArmed;
    accumulator_set<float, stats<tag::rolling_mean> > mBatteryVoltageAcc;

    WarningAlertState mState;


    QTreeWidgetItem *findDiagnosticItemByFullPath(const QString &fullPath);
    void updateDiagnosticTree(QString fullPath, const ptree::path_type &currentPathItem, const ptree &inTree, QTreeWidgetItem *parentTWIPtr);
    void levelStringToColor(const string &currentValue, QColor &bgColor);

public Q_SLOTS:
    void resetConfig(bool ttt);
    void applyConfig();
    void restoreDefaultConfig();

    void batteryState(float miliVolts);

    void insertLog(uint8_t level, const QString &timestamp, const QString &logLine);
    void diagnosticStatus(int8_t lvlv, boost::shared_ptr<ptree> diagTreePtr);
    void slotHeartBeat();
    void slotStateChanged(WarningAlertStates state, const string &messages);
    void updateBatteryIndicator();
};

} // namespace rviz_plugin_miner
