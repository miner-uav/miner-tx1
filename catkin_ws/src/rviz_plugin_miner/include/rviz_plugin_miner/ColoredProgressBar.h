#ifndef COLOREDPROGRESSBAR_H
#define COLOREDPROGRESSBAR_H

#include <QList>
#include <QPaintEvent>
#include <QPair>
#include <QProgressBar>
#include <QWidget>

using namespace std;

class ColoredProgressBar : public QProgressBar
{
    Q_OBJECT
  public:
    typedef tuple<int, int, QColor> range_t;
    typedef vector<range_t> ranges_t;

    explicit ColoredProgressBar(QWidget *parent = 0);
    ~ColoredProgressBar();
    void addRangeColor(int min_incl, int max_excl, const QColor &color);
    void resetColorRanges();

  protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void recalculateRangePixels();

    int mMarginX;
    int mMarginY;
    ranges_t mRanges;
    ranges_t mRangesPixels;
    float mPercentScale;
    float mPercentOffset;
};

#endif // COLOREDPROGRESSBAR_H
