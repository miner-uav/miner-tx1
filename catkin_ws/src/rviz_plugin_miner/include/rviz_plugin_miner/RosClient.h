#pragma once

#include "rviz_plugin_miner/RvizPanelConfig.h"

#include <QObject>

#include <rosgraph_msgs/Log.h>
#include <ros/ros.h>
#include <rviz/config.h>
#include <std_msgs/UInt8.h>
#include <sensor_msgs/BatteryState.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/property_tree/ptree.hpp>
#include <diagnostic_msgs/DiagnosticStatus.h>

namespace rviz_plugin_miner
{
using namespace boost::property_tree;

class RosClient : public QObject
{
    Q_OBJECT
  public:
    RosClient(RvizPanelConfig *config);
    ~RosClient();

  Q_SIGNALS:
    void signalFlightStatusChanged(bool armed);
    void sdkAuthorityGranted();
    void sdkAuthorityReleased();
    void batteryState(float miliVolts);
    void diagnosticStatus(int8_t lvlv, boost::shared_ptr<ptree>);
    void flightStatusArrived();

    void insertLog(uint8_t level,  const QString &timestamp,  const QString &logLine);

  public Q_SLOTS:
    void drone_arm();
    void drone_disarm();
    void requestAuthority();
    void releaseAuthority();
    void exitTeleop();

  protected:
    RvizPanelConfig *mConfig;
    ros::Subscriber mSubFlightStatus;
    ros::Subscriber mSubBatteryState;
    ros::Subscriber mSubSdkAuthorityStatus;
    ros::Subscriber mSubRosLog;
    ros::Subscriber mSubDiag;
    bool drone_armed_;
    bool sdk_authority_granted_;
    ros::NodeHandle nh_;
    ros::ServiceClient mSrvDroneArmClient;
    ros::ServiceClient mSrvSdkAuthorityClient;
    ros::ServiceClient mSrvNmpcExitTeleopClient;
    uint32_t mLogLevelFlag;


    void cb_dji_flight_status(const std_msgs::UInt8ConstPtr &statusPtr);
    void cb_dji_battery_state(const sensor_msgs::BatteryStateConstPtr &statePtr);
    void cb_sdk_authority_status(const std_msgs::UInt8ConstPtr &statusPtr);
    void cb_ros_log(const rosgraph_msgs::LogConstPtr &logMsgPtr);
    void cb_diagnostics(const diagnostic_msgs::DiagnosticStatusConstPtr &diagMsgPtr);

    void updateLogLevel(const string &levelName);

    QString FormatTime(const ros::Time &date_time);
};
}

