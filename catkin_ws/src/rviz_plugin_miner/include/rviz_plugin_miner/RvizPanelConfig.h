#pragma once

#include <QMap>
#include <QVariant>
#include <rviz/config.h>
#include <string>

namespace rviz_plugin_miner
{

using namespace std;

static const QString KEY_SRV_DJI_ARM                = "srv_arm_control";
static const QString KEY_SRV_SDK_AUTHORITY          = "srv_sdk_authority";
static const QString KEY_SRV_NMPC_EXIT_TELEOP       = "srv_nmpc_exit_teleop";
static const QString KEY_TOPIC_SDK_AUTHORITY_STATUS = "topic_authority_status";
static const QString KEY_TOPIC_FLIGHT_STATUS        = "topic_flight_status";
static const QString KEY_TOPIC_BATTERY_STATE        = "topic_battery_state";
static const QString KEY_UI_LOG_LEVEL               = "ui_log_level";
static const QString KEY_BAT_CELLS                  = "battery_cels";
static const QString KEY_BAT_CELL_MVOLT_WARN         = "battery_cel_volt_warn";
static const QString KEY_BAT_CELL_MVOLT_ERROR        = "battery_cel_volt_error";

class ConfigItem
{
  public:
    ConfigItem()
    {
    }
    ConfigItem(const ConfigItem &valueToCopy)
    {
        DefaultValue = valueToCopy.DefaultValue;
        ActiveValue  = valueToCopy.ActiveValue;
        WorkingValue = valueToCopy.WorkingValue;
    }
    ConfigItem(QVariant defaultValue)
    {
        DefaultValue = defaultValue;
        ActiveValue = defaultValue;
        WorkingValue = defaultValue;
    }
    QVariant getDefaultValue() const
    {
        return DefaultValue;
    }

    QVariant getActiveValue() const
    {
        return ActiveValue;
    }

    QVariant getWorkingValue() const
    {
        return WorkingValue;
    }

    /**
     * @brief setWorkingValue sets the Working copy of the value
     * @param value
     * @return true if change occured false othervise
     */
    bool setWorkingValue(const QVariant &value)
    {
        if (WorkingValue == value)
        {
            return false;
        }
        else
        {
            WorkingValue = value;
            return true;
        }
    }

    bool resetWorkingValueChanges()
    {
        if (WorkingValue == ActiveValue)
        {
            return false;
        }
        else
        {
            WorkingValue = ActiveValue;
            return true;
        }
    }

    bool isClean()
    {
        return (ActiveValue == WorkingValue);
    }

    /**
     * @brief setWorkingToDefaultValue
     * @return true if change was made false otherwise
     */
    bool setWorkingToDefaultValue()
    {
        return setWorkingValue(DefaultValue);
    }

    /**
     * @brief ApplyChange sets ActiValue to WorkingValue
     * @return true if change was made false otherwise
     */
    bool ApplyChange()
    {
        if (ActiveValue == WorkingValue)
        {
            return false;
        }
        else
        {
            ActiveValue = WorkingValue;
            return true;
        }
    }

  private:
    QVariant DefaultValue;
    QVariant ActiveValue;
    QVariant WorkingValue;
};

class RvizPanelConfig : public QObject
{
    Q_OBJECT
  public:
    RvizPanelConfig();
    void setFromRvizConfig(const rviz::Config &config, bool forceChangeNotification);
    void updateRvizConfig(rviz::Config &config) const;

    void setConfigItem(QString key, const QVariant &value);

    double getDoubleValue(QString key);
    int getIntValue(QString key);
    QString getStringValue(QString key);

    void resetToDefaults();
    void resetChanges();
    void applyChanges();
    void fireItemChangedForAll();

  Q_SIGNALS:
    void signalItemChanged(const QString &key, const QVariant &value);
    void signalItemCommited(const QString &key, const QVariant &value);
    void signalCleanStatus(bool clean);    
protected:
    void checkClean();
    bool lastCleanValue;
    QMap<QString, ConfigItem *> mConfigItems;
};
}
