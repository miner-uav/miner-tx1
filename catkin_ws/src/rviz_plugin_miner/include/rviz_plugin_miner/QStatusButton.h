#pragma once

#include <QObject>
#include <QPushButton>
#include <QStateMachine>
#include <QTimer>

class QStatusButton : public QPushButton
{
    Q_OBJECT
  public:
    explicit QStatusButton(QWidget *parent = nullptr);
    ~QStatusButton();

    void setStateNames(QString on, QString off, QString onToOff, QString offToOn);

  Q_SIGNALS:
    void enteredOff();
    void enteredOn();
    void enteredOffToOn();
    void enteredOnToOff();

  protected:
    void setupFsmStates();

  public Q_SLOTS:
    void slotSetOn();
    void slotSetOff();

  protected Q_SLOTS:
    void toggleStyle();
    void onEnteredOn();
    void onEnteredOff();
    void onEnteredOnToOff();
    void onEnteredOffToOn();

  protected:
  Q_SIGNALS:
    void signalSetOff();
    void signalSetOn();

  private:
    QStateMachine machine;
    QState stateOn;
    QState stateOff;
    QState stateOnToOff;
    QState stateOffToOn;
    QString stateNameOn;
    QString stateNameOff;
    QString stateNameOnToOff;
    QString stateNameOffToOn;

    QString styleOn;
    QString styleOff;
    QString *currentStyle;

    QTimer flashingTimer;

    int flashingInterval_;
};
