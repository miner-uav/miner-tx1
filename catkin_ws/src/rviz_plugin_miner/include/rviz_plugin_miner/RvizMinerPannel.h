#pragma once

#include <QWidget>

namespace rviz_plugin_miner
{

namespace Ui
{
class RvizMinerPannel;
}

class RvizMinerPannel : public QWidget
{
    Q_OBJECT

  public:
    explicit RvizMinerPannel(QWidget *parent = nullptr);
    ~RvizMinerPannel();

  public:
    Ui::RvizMinerPannel *ui;
};
}
