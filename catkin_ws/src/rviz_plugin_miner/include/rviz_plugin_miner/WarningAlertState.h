#ifndef WARNINGALERTSTATE_H
#define WARNINGALERTSTATE_H

#include <QMap>
#include <QObject>
#include <QSoundEffect>
#include <boost/thread/pthread/mutex.hpp>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <stdint.h>

namespace rviz_plugin_miner
{

static const QString STATE_COMPONENT_BATTERY_STATUS        = "Battery Status";
static const QString STATE_COMPONENT_BATTERY_VALUE        = "Battery Value";
static const QString STATE_COMPONENT_FLIGHT_ST      = "Flight Status";
static const QString STATE_COMPONENT_DIAGNOSTIC_SYS     = "Diagnostic System";
static const QString STATE_COMPONENT_DIAGNOSTIC_VAL = "Diagnostic Value";

// must match to the diagnostic_msgs::DiagnosticStatus
enum WarningAlertStates
{
    WAS_NORMAL  = diagnostic_msgs::DiagnosticStatus::OK,
    WAS_WARNING = diagnostic_msgs::DiagnosticStatus::WARN,
    WAS_ERROR   = diagnostic_msgs::DiagnosticStatus::ERROR,
    WAS_STALE   = diagnostic_msgs::DiagnosticStatus::STALE
};

using namespace std;
class WarningAlertState : public QObject
{
    Q_OBJECT
  public:
    WarningAlertState();
    ~WarningAlertState();
  Q_SIGNALS:
    void stateChanged(WarningAlertStates state, const string &messages);

  public Q_SLOTS:
    void setStatus(WarningAlertStates state, const QString &subsystem);
    void slotFlightStatusChanged(bool armed);
    void cancelSound();

  protected:
    boost::mutex mMutex;
    QMap<QString, WarningAlertStates> mComponentStatus;
    WarningAlertStates mCurrentState;
    QSoundEffect mSoundRedAlert;
    bool mPlaySound;
    bool mFlightArmed;

    void evaluateState();
    void setState(vector<string> warningMsgs, vector<string> errorMsgs, WarningAlertStates newState);
    string levelToString(WarningAlertStates level);
};

} // namespace rviz_plugin_miner

#endif // WARNINGALERTSTATE_H
