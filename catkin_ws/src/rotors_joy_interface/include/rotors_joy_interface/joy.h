/*
 * Copyright 2015 Fadri Furrer, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Michael Burri, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Mina Kamel, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Janosch Nikolic, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Markus Achtelik, ASL, ETH Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef ROTORS_JOY_INTERFACE_JOY_H_
#define ROTORS_JOY_INTERFACE_JOY_H_

#include <geometry_msgs/PoseStamped.h>
#include <mav_msgs/RollPitchYawrateThrust.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/UInt8.h>

// DJI SDK includes
#include <dji_sdk/dji_sdk.h>
#include <dji_sdk/Activation.h>
#include <dji_sdk/DroneTaskControl.h>
#include <dji_sdk/SDKControlAuthority.h>
#include <dji_sdk/DroneArmControl.h>
#include <dji_sdk/MissionWpAction.h>
#include <dji_sdk/MissionHpAction.h>
#include <dji_sdk/MissionWpUpload.h>
#include <dji_sdk/MissionHpUpload.h>
#include <dji_sdk/MissionHpUpdateRadius.h>
#include <dji_sdk/MissionHpUpdateYawRate.h>

// SDK core library
#include <djiosdk/dji_vehicle.hpp>

struct AxesState {
  bool roll;
  bool pitch;
  bool thrust;
  bool yaw_rate;
  bool control;
};

struct AxesChannels {
  int roll;
  int pitch;
  int thrust;
  int yaw_rate;
  int control;
};

struct Buttons {
  int takeoff;
  int land;
  int ctrl_enable;
  int ctrl_disable;
};

struct AxesValues {
  double roll;
  double pitch;
  double yaw_rate;
  double thrust;
  double control;
};

enum ControlMode {
    ControlMode_Disabled = 0,
    ControlMode_Buttons,
    ControlMode_Axe
};

enum JoyState {
    JoyState_WaitingForFirstMessage = 0,
    JoyState_WaitingForAllChanges,
    JoyState_Ready
};

class Joy {
  typedef sensor_msgs::Joy::_buttons_type ButtonType;
  const double AXES_CHANGE_THRESHOLD = 0.01;

 private:
  ros::NodeHandle nh_;
  ros::Publisher ctrl_pub_;
  ros::Subscriber joy_sub_;
  ros::Subscriber flight_status_sub_;
  ros::ServiceClient sdk_ctrl_authority_service;
  ros::ServiceClient sdk_drone_arm_service;


  std::string namespace_;
  std::string service_control_authority;
  std::string service_drone_arm;
  std::string topic_roll_pitch_yawrate_thrust;

  AxesChannels axes_;
  Buttons buttons_;

  mav_msgs::RollPitchYawrateThrust control_msg_;
  geometry_msgs::PoseStamped pose_;

  AxesValues max_;
  AxesValues min_;
  AxesValues scale_;
  AxesValues offset_;
  AxesValues first_values_;
  JoyState joy_state_;
  AxesState axes_changed_;

  ControlMode ctrl_mode_;
  DJISDK::FlightStatus flight_ftatus_;

  int control_debounce;
  double current_yaw_vel_;
  double control_threshold_;

  bool has_control_authority;
  bool is_fixed_wing_;

  void StopMav();

  void MotorArm();
  void MotorDisArm();
  void JoyCallback(const sensor_msgs::JoyConstPtr& msg);
  void FlightStatusCallBack(const std_msgs::UInt8 &msg);
  void Publish();
  void ObtainControl(bool obtain);
  double ScaleAxes(double x, double scale, double offset, double ymin, double ymax);

public:
  Joy();
};

#endif // ROTORS_JOY_INTERFACE_JOY_H_
