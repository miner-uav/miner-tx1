/*
 * Copyright 2015 Fadri Furrer, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Michael Burri, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Mina Kamel, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Janosch Nikolic, ASL, ETH Zurich, Switzerland
 * Copyright 2015 Markus Achtelik, ASL, ETH Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// note there is conflice os "STATUS" DJI macro and constant in default_topics. the order of includes is important
#include <mav_msgs/default_topics.h>
#include "rotors_joy_interface/joy.h"
#include <mav_msgs/RollPitchYawrateThrust.h>

Joy::Joy()
{
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  pnh.param<std::string>("service_control_authority", service_control_authority, "dji_sdk/sdk_control_authority");
  pnh.param<std::string>("service_drone_arm", service_drone_arm, "dji_sdk/drone_arm_control");
  pnh.param<std::string>("topic_roll_pitch_yawrate_thrust", topic_roll_pitch_yawrate_thrust, std::string(mav_msgs::default_topics::COMMAND_ROLL_PITCH_YAWRATE_THRUST));

  ctrl_pub_ = nh_.advertise<mav_msgs::RollPitchYawrateThrust> (topic_roll_pitch_yawrate_thrust, 10);

  sdk_ctrl_authority_service = nh_.serviceClient<dji_sdk::SDKControlAuthority> (service_control_authority);
  sdk_drone_arm_service = nh_.serviceClient<dji_sdk::DroneArmControl> (service_drone_arm);


  control_msg_.header.frame_id = "uav";
  control_msg_.roll = 0;
  control_msg_.pitch = 0;
  control_msg_.yaw_rate = 0;
  control_msg_.thrust.x = 0;
  control_msg_.thrust.y = 0;
  control_msg_.thrust.z = 0;

  axes_changed_.control =false;
  axes_changed_.pitch =false;
  axes_changed_.roll =false;
  axes_changed_.thrust =false;
  axes_changed_.yaw_rate =false;

  current_yaw_vel_ = 0;
  control_debounce = 0;
  has_control_authority = false;
  joy_state_ = JoyState_WaitingForFirstMessage;

  pnh.param("axis_roll", axes_.roll, 3);
  pnh.param("axis_pitch", axes_.pitch, 4);
  pnh.param("axis_thrust", axes_.thrust, 1);
  pnh.param("axis_yaw_rate", axes_.yaw_rate, 0);
  pnh.param("axis_control", axes_.control, 0);

  pnh.param("control_threshold", control_threshold_, 0.9);

  pnh.param("max_roll", max_.roll, 10.0 * M_PI / 180.0);  // [rad]
  pnh.param("max_pitch", max_.pitch, 10.0 * M_PI / 180.0);  // [rad]
  pnh.param("max_yaw_rate", max_.yaw_rate, 45.0 * M_PI / 180.0);  // [rad/s]
  pnh.param("max_thrust", max_.thrust, 100.0);  // [N]
  pnh.param("max_control", max_.control, 1.0);  // [N]

  pnh.param("min_roll", min_.roll, -10.0 * M_PI / 180.0);  // [rad]
  pnh.param("min_pitch", min_.pitch, -10.0 * M_PI / 180.0);  // [rad]
  pnh.param("min_yaw_rate", min_.yaw_rate, -45.0 * M_PI / 180.0);  // [rad/s]
  pnh.param("min_thrust", min_.thrust, 3.0);  // [%]
  pnh.param("min_control", min_.control, -1.0);  // [N]

  pnh.param("scale_roll", scale_.roll, 1.0);
  pnh.param("scale_pitch", scale_.pitch, 1.0);
  pnh.param("scale_yaw_rate", scale_.yaw_rate, 1.0);
  pnh.param("scale_thrust", scale_.thrust, 1.0);
  pnh.param("scale_control", scale_.control, 1.0);

  pnh.param("offset_roll", offset_.roll, 0.0);
  pnh.param("offset_pitch", offset_.pitch, 0.0);
  pnh.param("offset_yaw_rate", offset_.yaw_rate, 0.0);
  pnh.param("offset_thrust", offset_.thrust, 0.0);
  pnh.param("offset_control", offset_.control, 0.0);


  pnh.param("is_fixed_wing", is_fixed_wing_, false);

  pnh.param("button_ctrl_enable", buttons_.ctrl_enable, 5);
  pnh.param("button_ctrl_disable", buttons_.ctrl_disable, 6);

  std::string temp;
  pnh.param<std::string>("control_mode", temp, "disabled");
  std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
  if(temp.compare("axe") == 0)
  {
      this->ctrl_mode_ = ControlMode_Axe;
      ROS_INFO("Control MODE is Axe");
  }
  else if(temp.compare("buttons") == 0)
  {
      this->ctrl_mode_ = ControlMode_Buttons;
      ROS_INFO("Control MODE is Buttons");
  }
  else
  {
      this->ctrl_mode_ = ControlMode_Disabled;
      ROS_INFO("Control MODE is Disabled");
  }

  pnh.param("button_takeoff", buttons_.takeoff, 7);
  pnh.param("button_land", buttons_.land, 8);

  if(max_.control < min_.control) ROS_ERROR("Invalir min-max range for control");
  if(max_.pitch < min_.pitch) ROS_ERROR("Invalir min-max range for pitch");
  if(max_.roll < min_.roll) ROS_ERROR("Invalir min-max range for roll");
  if(max_.thrust < min_.thrust) ROS_ERROR("Invalir min-max range for thrust");
  if(max_.yaw_rate < min_.yaw_rate) ROS_ERROR("Invalir min-max range for yaw_rate");

  namespace_ = nh_.getNamespace();
  flight_status_sub_ = nh_.subscribe("/dji_sdk/flight_status", 10, &Joy::FlightStatusCallBack, this);
  joy_sub_ = nh_.subscribe("joy", 10, &Joy::JoyCallback, this);
}

void Joy::StopMav() {
  control_msg_.roll = 0;
  control_msg_.pitch = 0;
  control_msg_.yaw_rate = 0;
  control_msg_.thrust.x = 0;
  control_msg_.thrust.y = 0;
  control_msg_.thrust.z = 0;
}

void Joy::FlightStatusCallBack(const std_msgs::UInt8& msg)
{
    DJISDK::FlightStatus new_state = static_cast<DJISDK::FlightStatus>(msg.data);
    if(flight_ftatus_ != new_state)
    {
        switch (new_state) {
        case DJISDK::STATUS_STOPPED:
            ROS_INFO("FLIGHT STATUS changed to STOPED");
            flight_ftatus_ = new_state;
            break;
        case DJISDK::STATUS_IN_AIR:
            ROS_INFO("FLIGHT STATUS changed to IN_AIR");
            flight_ftatus_ = new_state;
            break;
        case DJISDK::STATUS_ON_GROUND:
            ROS_INFO("FLIGHT STATUS changed to ON_GROUND");
            flight_ftatus_ = new_state;
            break;
        default:
            ROS_ERROR("Invalid Flight Stauts");
            break;
        }
    }
}

void Joy::JoyCallback(const sensor_msgs::JoyConstPtr& msg)
{

    switch (joy_state_) {
    case JoyState_WaitingForFirstMessage:
        //store first values
        ROS_WARN("SAFETY: Waiting for all axes to change");
        first_values_.control = msg->axes[axes_.control];
        first_values_.pitch = msg->axes[axes_.pitch];
        first_values_.roll = msg->axes[axes_.roll];
        first_values_.thrust = msg->axes[axes_.thrust];
        first_values_.yaw_rate = msg->axes[axes_.yaw_rate];
        joy_state_ = JoyState_WaitingForAllChanges;
        return;
    case JoyState_WaitingForAllChanges:
        if(!axes_changed_.control && abs(first_values_.control - msg->axes[axes_.control]) > AXES_CHANGE_THRESHOLD)
        {
            ROS_INFO("Control Axes has changed");
            axes_changed_.control = true;
        }
        if(!axes_changed_.pitch && abs(first_values_.pitch - msg->axes[axes_.pitch]) > AXES_CHANGE_THRESHOLD)
        {
            ROS_INFO("Pitch Axes has changed");
           axes_changed_.pitch = true;
        }
        if(!axes_changed_.roll && abs(first_values_.roll - msg->axes[axes_.roll]) > AXES_CHANGE_THRESHOLD)
        {
            ROS_INFO("Roll Axes has changed");
            axes_changed_.roll = true;
        }
        if(!axes_changed_.thrust && abs(first_values_.thrust - msg->axes[axes_.thrust]) > AXES_CHANGE_THRESHOLD)
        {
            ROS_INFO("Thrust Axes has changed");
            axes_changed_.thrust = true;
        }
        if(!axes_changed_.yaw_rate && abs(first_values_.yaw_rate - msg->axes[axes_.yaw_rate]) > AXES_CHANGE_THRESHOLD)
        {
            ROS_INFO("Yaw Reate Axes has changed");
            axes_changed_.yaw_rate = true;
        }

        if(axes_changed_.control && axes_changed_.pitch && axes_changed_.roll && axes_changed_.thrust && axes_changed_.yaw_rate)
        {
            ROS_INFO("All axes has changed - Ready");
            joy_state_ = JoyState_Ready;
        }
        return;
    case JoyState_Ready:
        break;
    default:
        ROS_ERROR_THROTTLE(1.0, "Invalid Joy State");
        return;
    }

    if(this->ctrl_mode_ == ControlMode_Buttons)
    {
        ROS_DEBUG("ControlMode_Buttons");
        if (msg->buttons[buttons_.ctrl_enable])
        {
          control_debounce++;
        }
        else if (msg->buttons[buttons_.ctrl_disable])
        {
          control_debounce--;
        }
        else
        {
            control_debounce = 0;
        }
    }
    else if(this->ctrl_mode_ == ControlMode_Axe)
    {
        ROS_DEBUG("ControlMode_Axe");
        double control_axes = ScaleAxes(msg->axes[axes_.control], scale_.control, offset_.control, min_.control, max_.control);
        //ROS_INFO("Control IN:%f; OUT %f", msg->axes[axes_.control], control_axes);
        if(control_axes > control_threshold_)
        {
            control_debounce = 21;
        }
        else
        {
            control_debounce = -6;
        }
    }
    else
    {
        ROS_DEBUG("ControlMode_Disabled");
        control_debounce = 0;
    }


    if(control_debounce > 20)
    {
        ObtainControl(true);
    }

    if(control_debounce < -5)
    {
        ObtainControl(false);
    }
    if (!has_control_authority)
    {
        ROS_DEBUG("We do not have authority");
        return;
    }

  control_msg_.roll =     ScaleAxes(msg->axes[axes_.roll],     scale_.roll,     offset_.roll,     min_.roll,     max_.roll);
  control_msg_.pitch =    ScaleAxes(msg->axes[axes_.pitch],    scale_.pitch,    offset_.pitch,    min_.pitch,    max_.pitch);
  control_msg_.yaw_rate = ScaleAxes(msg->axes[axes_.yaw_rate], scale_.yaw_rate, offset_.yaw_rate, min_.yaw_rate, max_.yaw_rate);



  if (is_fixed_wing_)
  {
    control_msg_.thrust.x = ScaleAxes(msg->axes[axes_.thrust], scale_.thrust, offset_.thrust, min_.thrust, max_.thrust);
  }
  else
  {
    control_msg_.thrust.z = ScaleAxes(msg->axes[axes_.thrust], scale_.thrust, offset_.thrust, min_.thrust, max_.thrust);
  }

  control_msg_.header.stamp = ros::Time::now();
  Publish();
}

double Joy::ScaleAxes(double x, double scale, double offset, double ymin, double ymax)
{
    double y = x * scale + offset;
    y = y < ymin ? ymin : y;
    y = y > ymax ? ymax : y;
    return y;
}

void Joy::Publish() {
    ROS_DEBUG("Publishing....");
  ctrl_pub_.publish(control_msg_);
}

void Joy::MotorArm()
{
    dji_sdk::DroneArmControl drone_arm;
    drone_arm.request.arm = true;
    sdk_drone_arm_service.call(drone_arm);
    if(!drone_arm.response.result)
    {
      ROS_ERROR("Motor Arm control call failed!");
    }
    else
    {
        ROS_INFO("Motor Arm control call success.");
    }
}

void Joy::MotorDisArm()
{
    dji_sdk::DroneArmControl drone_arm;
    drone_arm.request.arm = false;
    sdk_drone_arm_service.call(drone_arm);
    if(!drone_arm.response.result)
    {
      ROS_ERROR("Motor Dis Arm control call failed!");
    }
    else
    {
        ROS_INFO("Motor Dis Arm control call success.");
    }
}

void Joy::ObtainControl(bool obtain)
{
    //no change
    if(obtain == has_control_authority)
    {
        return;
    }
    dji_sdk::SDKControlAuthority authority;
    authority.request.control_enable=obtain;
    sdk_ctrl_authority_service.call(authority);

    //testing
    //authority.response.result = true;

    if(!authority.response.result)
    {
      ROS_ERROR("%s control failed!", obtain ? "obtain" : "release");
    }
    else
    {
      has_control_authority= obtain;
      ROS_INFO("%s control succes!", obtain ? "obtain" : "release");
    }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "rotors_joy_interface");
  Joy joy;

  ros::spin();

  return 0;
}
