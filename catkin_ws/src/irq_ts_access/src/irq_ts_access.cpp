#include "irq_ts_access/irq_ts_access.h"

#include <fcntl.h>
#include <stdexcept>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h> // for close

namespace irq_ts_access
{

IrqTsAccess::IrqTsAccess(const string devName)
    : mDeviceName(devName)
    , mFD(-1)
{
}

void IrqTsAccess::OpenRw()
{
    mFD = open(mDeviceName.c_str(), O_RDWR);

    if (mFD < 0)
    {
        throw invalid_argument("Failed to open device, check kernel logs dmesg, "
                               "device name, permissions...");
    }
}

void IrqTsAccess::Open()
{
    mFD = open(mDeviceName.c_str(), O_RDONLY);

    if (mFD < 0)
    {
        throw invalid_argument("Failed to open device, check kernel logs dmesg, "
                               "device name, permissions...");
    }
}

int IrqTsAccess::Close()
{
    int ret;
    ret = close(mFD);
    mFD = -1;
    return ret;
}

int IrqTsAccess::Poll()
{
}

/**
 * @brief IrqTsAccess::Read - this is blocking call!
 * @param outData
 * @return:
 *     positive number: number of entries returned
 *     0: - read timeout
 *     -%ERESTARTSYS: - read interrupted by signal
 */
int IrqTsAccess::Read(std::vector<boost::shared_ptr<IrqTsAccessTimestamp_t> > &outData)
{
    irq_ts_read_t retData;
    size_t bytesRead = read(mFD, &retData, sizeof(retData));

    timeval tim;
    gettimeofday(&tim, NULL);

    if (bytesRead == sizeof(retData))
    {
        for (int i = 0; i < IRQ_TS_PIN_COUNT; ++i)
        {
            if (retData[i].has_time)
            {

                // printf("IrqTsAccess::Read seq: %d",retData[i].seq );
                auto entry      = boost::make_shared<IrqTsAccessTimestamp_t>();
                entry->entry_id = retData[i].entry_id;
                entry->seq      = retData[i].seq;
                entry->sec      = retData[i].sec;
                entry->nsec     = retData[i].nsec;
                entry->us_sec   = tim.tv_sec;
                entry->us_usec  = tim.tv_usec;
                entry->has_time = retData[i].has_time;
                outData.push_back(entry);
            }
        }
        return outData.size();
    }
    return bytesRead;
}

bool IrqTsAccess::Read(uint32_t entry_id, long *seconds, long *nano_seconds, uint32_t *seq)
{
    irq_ts_read_t retData;
    size_t bytesRead = read(mFD, &retData, sizeof(retData));

    if (bytesRead == sizeof(retData))
    {
        for (int i = 0; i < IRQ_TS_PIN_COUNT; ++i)
        {
            if (retData[i].has_time && retData[i].entry_id == entry_id)
            {

                *seq          = retData[i].seq;
                *seconds      = retData[i].sec;
                *nano_seconds = retData[i].nsec;
                return true;
            }
        }
    }
    return false;
}

int IrqTsAccess::Report(irq_ts_ioctl_report_t *reportPtr)
{

    int ret = 0;

    ret = ioctl(mFD, IRQ_TS_IOCTL_REPORT, reportPtr);

    return ret;
}

void IrqTsAccess::AddPin(uint8_t entruID, uint32_t gpioPin, bool risingEdge, std::string name)
{
    if (name.length() > IRQ_TS_IRQ_NAME_MAX_LEN)
    {
        throw invalid_argument("Failed to add pin, name too long");
    }

    irq_ts_ioctl_add_t pinSet;
    pinSet.entry_id    = entruID;
    pinSet.gpio_pin    = gpioPin;
    pinSet.rising_edge = risingEdge;
    strncpy(pinSet.name, name.c_str(), IRQ_TS_IRQ_NAME_MAX_LEN);

    if (ioctl(mFD, IRQ_TS_IOCTL_ADD_PIN, &pinSet))
    {
        throw invalid_argument("Failed to add pin, check kernel logs dmesg,device name, permissions...");
    }
}
}
