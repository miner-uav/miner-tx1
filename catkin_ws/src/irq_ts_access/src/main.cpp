#include "irq_ts_access/irq_ts_access.h"
#include <chrono>
#include <stdio.h>
#include <string.h>
#include <thread>

int main(int argc, char *argv[])
{
    irq_ts_access::IrqTsAccess irqTs("/dev/irq_ts");

    irqTs.Open();

    irqTs.AddPin(0, 314, true, "PIN314");
    uint32_t sequence;

    std::vector<boost::shared_ptr<irq_ts_access::IrqTsAccess::IrqTsAccessTimestamp_t>> timeStamps;
    while (1 == 1)
    {
        irqTs.Read(timeStamps);
        for (auto ts : timeStamps)
        {
            if (ts->has_time)
            {
                if(sequence != ts->seq)
                {
                    printf("WARNING LOST SEQ:%d\n", ts->seq - sequence);
                    sequence = ts->seq;
                }
                sequence++;
                double time    = ts->sec + (ts->nsec / 1.0e9);
                double us_time = ts->us_sec + (ts->us_usec/ 1.0e6);
                double delta   = (us_time - time) * 1.0e3;
                ;
                printf("Entry %d seq: %d; value: %.6f [sec]; us time: %.6f [sec]; delta: %.6f [msec]\n",
                       ts->entry_id,
                       ts->seq,
                       time,
                       us_time,
                       delta);
            }
        }
    }
    irqTs.Close();
    return 0;
}
