#ifndef IRQ_TS_H
#define IRQ_TS_H

#include <linux/fs.h>
#include <linux/types.h>
/*
 * ===============================================
 *             IRQ_TS Data Structures
 * ===============================================
 */

/* Convienient constant name */
#define IRQ_TS_MODULE_NAME "irq_ts"

//#define IRQ_TS_BLOCKING_CALL
#define IRQ_TS_DEBUG_PIN 313

#define IRQ_TS_PIN_COUNT 2
#define IRQ_TS_IRQ_NAME_MAX_LEN 15
#define IRQ_TS_IRQ_NAME_MIN_LEN 4
#define IRQ_TS_IRQ_READ_TIMEOUT_DEFAULT_MSEC 200
/*
 * ===============================================
 *             Public API Functions
 * ===============================================
 */

typedef struct irq_ts_read_entry_s
{
    long sec;
    long nsec;
    /**
     * @brief entry_id must be less than IRQ_TS_PIN_COUNT
     */
    unsigned int entry_id;
    unsigned int seq;
    bool has_time;
    /**
     * @brief timestamp_us timestamp in micro sec
     */

} irq_ts_read_entry_t;

typedef irq_ts_read_entry_t irq_ts_read_t[IRQ_TS_PIN_COUNT];

/*
 * There typically needs to be a struct definition for each flavor of
 * IOCTL call.
 */
typedef struct irq_ts_ioctl_add_s
{
    /**
     * @brief entry_id must be less than IRQ_TS_PIN_COUNT
     */
    unsigned entry_id;
    /**
     * @brief gpio_number gpio pin mumber for the irq
     */
    unsigned gpio_pin;

    /**
     * @brief rising_edge - true - rising, false falling
     */
    bool rising_edge; //edge;

    /**
     * @brief name - null terminated name of irq (+1 for null)
     */
    char name[IRQ_TS_IRQ_NAME_MAX_LEN + 1];

} irq_ts_ioctl_add_t;

typedef struct irq_ts_ioctl_remove_s
{
    /**
     * @brief entry_id must be less than IRQ_TS_PIN_COUNT
     */
    unsigned entry_id;

} irq_ts_ioctl_remove_t;

typedef struct irq_ts_ioctl_set_timeout_s
{
    /**
     * @brief timeout_milisec sets read timeout in mili seconds. default is %IRQ_TS_IRQ_READ_TIMEOUT_DEFAULT_MSEC
     */
    unsigned timeout_milisec;

} irq_ts_ioctl_set_timeout_t;

typedef struct irq_ts_ioctl_report_s
{
    /**
     * @brief entry_id must be less than IRQ_TS_PIN_COUNT
     */
    unsigned entry_id;
    /**
     * @brief gpio_number gpio pin mumber for the irq
     */
    unsigned gpio_pin;

    /**
     * @brief gpio_value value at the time of report, if pin is enabled.
     */
    unsigned gpio_value;

    /**
     * @brief edge type of interrupt: IRQF_TRIGGER_XXX see linux/interrupt.h
     */
    bool rising_edge;

    /**
     * @brief enabled: 1 - enabled; 0 - disabled
     */
    bool enabled;

    /**
     * @brief name - null terminated name of irq (+1 for null)
     */
    char name[IRQ_TS_IRQ_NAME_MAX_LEN + 1];

} irq_ts_ioctl_report_item_t;

typedef irq_ts_ioctl_report_item_t irq_ts_ioctl_report_t[IRQ_TS_PIN_COUNT];

/*
 * This generic union allows us to make a more generic IOCTRL call
 * interface. Each per-IOCTL-flavor struct should be a member of this
 * union.
 */

typedef union irq_ts_ioctl_param_u {
    irq_ts_ioctl_add_t add;
    irq_ts_ioctl_set_timeout_t set_timeout;
    irq_ts_ioctl_remove_t remove;
    irq_ts_ioctl_report_t report;
} irq_ts_ioctl_param_union_t;

/*
 * Used by _IOW to create the unique IOCTL call numbers. It appears
 * that this is supposed to be a single character from the examples I
 * have looked at so far.
 */
#define IRQ_TS_MAGIC 'm'

/*
 * For each flavor of IOCTL call you will need to make a macro that
 * calls the _IOW() macro. This macro is just a macro that creates a
 * unique ID for each type of IOCTL call. It uses a combination of bit
 * shifting and OR-ing of each of these arguments to create the
 * (hopefully) unique constants used for IOCTL command values.
 */
#define IRQ_TS_IOCTL_ADD_PIN _IOW(IRQ_TS_MAGIC, 1, irq_ts_ioctl_add_t)
#define IRQ_TS_IOCTL_SET_TIMEOUT _IOW(IRQ_TS_MAGIC, 2, irq_ts_ioctl_set_timeout_t)
#define IRQ_TS_IOCTL_REMOVE_ENTRY _IOW(IRQ_TS_MAGIC, 3, irq_ts_ioctl_remove_t)
#define IRQ_TS_IOCTL_REPORT _IOR(IRQ_TS_MAGIC, 4, irq_ts_ioctl_report_t)

#endif /* IRQ_TS_H */
