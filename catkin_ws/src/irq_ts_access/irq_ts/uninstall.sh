#!/bin/bash

MODULENAME=irq_ts

if [[ $EUID -ne 0 ]] ;
then
   echo "This install script must be run as root" 1>&2
   exit 1
fi

lsmod | grep $MODULENAME > /dev/null
if [ $? -eq 0 ]
then
  echo "removing module..."
  sudo rmmod $MODULENAME
fi
echo removing udev rules
sudo rm -f /etc/udev/rules.d/99-irq_ts_udev.rules
sudo udevadm control --reload-rules
