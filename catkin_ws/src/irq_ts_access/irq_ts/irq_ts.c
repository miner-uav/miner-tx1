#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/ktime.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/unistd.h>

#include "include/linux/irq_ts.h"

typedef struct irq_ts_dev_pin_config_entry_s
{
    unsigned gpio_pin;
    bool rising_edge; // edge
    bool enabled;
    unsigned int seq;
    bool polled;
    int irq_num;
    struct timespec last_ts;
    char name[IRQ_TS_IRQ_NAME_MAX_LEN + 1];

} irq_ts_dev_pin_config_entry_t;

typedef struct irq_ts_dev_data_s
{
    unsigned long read_timeout_jiffies;
    spinlock_t spin_irq_lock;
    struct mutex ioctl_mutex;
#ifdef IRQ_TS_BLOCKING_CALL
    wait_queue_head_t read_blocking_queue;
#endif

#ifdef IRQ_TS_DEBUG_PIN
    bool debug_pin_value;
#endif
    volatile bool have_new_data;
    irq_ts_dev_pin_config_entry_t pins[IRQ_TS_PIN_COUNT];
} irq_ts_dev_data_t;

static irq_ts_dev_data_t irq_ts_dev;

#ifdef IRQ_TS_DEBUG_PIN
static void _irq_ts_dev_setupDebugPin(void)
{
    bool ret_val;
    // check if pin is valid
    ret_val = gpio_is_valid(IRQ_TS_DEBUG_PIN);
    if (!ret_val)
    {
        pr_err("setupDebugPin: gpio_is_valid failed: %d\n", ret_val);
        goto fail_exit;
    }
    ret_val = gpio_request(IRQ_TS_DEBUG_PIN, "irs_ts_debug");
    if (ret_val != 0)
    {
        pr_err("setupDebugPin: gpio_request failed: %d\n", ret_val);
        goto fail_exit;
    }
    pr_debug("setupDebugPin: gpio_request success\n");

    ret_val = gpio_direction_output(IRQ_TS_DEBUG_PIN, irq_ts_dev.debug_pin_value);
    if (ret_val != 0)
    {
        pr_err("setupDebugPin: gpio_direction_output failed: %d\n", ret_val);
        goto fail_remove_pin;
    }
    pr_debug("setupDebugPin: gpio_direction_output success\n");

    ret_val = gpio_export(IRQ_TS_DEBUG_PIN, false);
    if (ret_val != 0)
    {
        pr_err("setupDebugPin: gpio_export failed: %d\n", ret_val);
        goto fail_remove_pin;
    }
    return;

fail_remove_pin:
    pr_debug("setupDebugPin:  gpio_free: %d\n", IRQ_TS_DEBUG_PIN);
    gpio_free(IRQ_TS_DEBUG_PIN);
fail_exit:
    return;
}
static void _irq_ts_dev_releaseDebugPin(void)
{
    pr_debug("releaseDebugPin:  gpio_free: %d...\n", IRQ_TS_DEBUG_PIN);
    gpio_free(IRQ_TS_DEBUG_PIN);
}
static void _irq_ts_dev_toggleDebugPin(void)
{
    irq_ts_dev.debug_pin_value = !irq_ts_dev.debug_pin_value;
    gpio_set_value(IRQ_TS_DEBUG_PIN, irq_ts_dev.debug_pin_value);
}

#define DEBUG_TOGLE_PIN() _irq_ts_dev_toggleDebugPin()
#define DEBUG_SETUP_PIN() _irq_ts_dev_setupDebugPin()
#define DEBUG_RELEASE_PIN() _irq_ts_dev_releaseDebugPin()

#else
#define DEBUG_TOGLE_PIN()
#define DEBUG_SETUP_PIN()
#define DEBUG_RELEASE_PIN()
#endif

static irq_handler_t irq_ts_dev_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
    unsigned long s;

    irq_ts_dev_pin_config_entry_t *config_entryPtr = (irq_ts_dev_pin_config_entry_t *)dev_id;
    // ignore the noise
    if (gpio_get_value(config_entryPtr->gpio_pin) != config_entryPtr->rising_edge)
    {
        return 0;
    }
    spin_lock_irqsave(&irq_ts_dev.spin_irq_lock, s);
    config_entryPtr->seq++;
    DEBUG_TOGLE_PIN();

#ifdef _POSIX_TIMERS
    clock_gettime(CLOCK_REALTIME, &config_entryPtr->last_ts);
    pr_debug("irq_ts_dev_irq_handler clock_gettime ts:%ld [sec]\n", config_entryPtr->last_ts.tv_sec);
#else
    ktime_get_real_ts(&config_entryPtr->last_ts);
// pr_debug("irq_ts_dev_irq_handler ktime_get_real_ts diff ts:%lld [us]\n", config_entryPtr->diff_us);
#endif
    irq_ts_dev.have_new_data = true;

    spin_unlock_irqrestore(&irq_ts_dev.spin_irq_lock, s);
#ifdef IRQ_TS_BLOCKING_CALL
    wake_up_interruptible(&irq_ts_dev.read_blocking_queue);
#endif

    return (irq_handler_t)IRQ_HANDLED;
}

static int irq_ts_dev_add_pin_monitor(const irq_ts_ioctl_add_t *gpio_add_entry, irq_ts_dev_pin_config_entry_t *config_entryPtr)
{
    int ret_val;
    int irq_num;
    size_t length;
    long irq_flag;
    pr_debug("irq_ts_dev_add_pin_monitor trying to setup gpio pin: %d; edge: %s; name:%s\n", gpio_add_entry->gpio_pin, gpio_add_entry->rising_edge ? "RISING" : "FALLING",
             gpio_add_entry->name);

    // check entry_id in within limits
    if (gpio_add_entry->entry_id >= IRQ_TS_PIN_COUNT)
    {
        ret_val = -EINVAL;
        pr_err("irq_ts_dev_add_pin_monitor Invalid entry_id; max is: %d\n", IRQ_TS_PIN_COUNT);
        goto fail_exit;
    }

    // check the name string lenght
    length = strlen(gpio_add_entry->name);
    if (length > IRQ_TS_IRQ_NAME_MAX_LEN)
    {
        ret_val = -EINVAL;
        pr_err("irq_ts_dev_add_pin_monitor name too long; max is %d\n", IRQ_TS_IRQ_NAME_MAX_LEN);
        goto fail_exit;
    }
    if (length < IRQ_TS_IRQ_NAME_MIN_LEN)
    {
        ret_val = -EINVAL;
        pr_err("irq_ts_dev_add_pin_monitor name too short min is %d\n", IRQ_TS_IRQ_NAME_MIN_LEN);
        goto fail_exit;
    }

    // check if it is enabled
    if (irq_ts_dev.pins[gpio_add_entry->entry_id].enabled)
    {
        ret_val = -EINVAL;
        pr_err("irq_ts_dev_add_pin_monitor entry_id already occupied");
        goto fail_exit;
    }

    // check if pin is valid
    ret_val = gpio_is_valid(gpio_add_entry->gpio_pin);
    if (!ret_val)
    {
        pr_err("irq_ts_dev_add_pin_monitor: gpio_is_valid failed: %d\n", ret_val);
        goto fail_exit;
    }

    //
    //
    // the entry is valid lets proceed with setting up gpio and irq
    //
    //

    ret_val = gpio_request(gpio_add_entry->gpio_pin, gpio_add_entry->name);
    if (ret_val != 0)
    {
        pr_err("irq_ts_dev_add_pin_monitor: gpio_request failed: %d\n", ret_val);
        goto fail_exit;
    }
    pr_debug("irq_ts_dev_add_pin_monitor: gpio_request success\n");

    ret_val = gpio_direction_input(gpio_add_entry->gpio_pin);
    if (ret_val != 0)
    {
        pr_err("irq_ts_dev_add_pin_monitor: gpio_direction_input failed: %d\n", ret_val);
        goto fail_remove_pin;
    }

    pr_debug("irq_ts_dev_add_pin_monitor: gpio_direction_input success\n");

    ret_val = gpio_export(gpio_add_entry->gpio_pin, false);
    if (ret_val != 0)
    {
        pr_err("irq_ts_dev_add_pin_monitor: gpio_export failed: %d\n", ret_val);
        goto fail_remove_pin;
    }
    pr_debug("irq_ts_dev_add_pin_monitor: gpio_export success\n");

    ret_val = gpio_to_irq(gpio_add_entry->gpio_pin);
    if (ret_val < 0)
    {
        pr_err("irq_ts_dev_add_pin_monitor: gpio_to_irq failed: %d\n", ret_val);
        goto fail_unexport;
    }
    else
    {
        irq_num = ret_val;
        pr_debug("irq_ts_dev_add_pin_monitor: gpio_to_irq success; irq:%d\n", irq_num);
    }

    if (gpio_add_entry->rising_edge)
    {
        irq_flag = IRQF_TRIGGER_RISING;
    }
    else
    {
        irq_flag = IRQF_TRIGGER_FALLING;
    }
    ret_val = request_irq(irq_num,                               // The interrupt number requested
                          (irq_handler_t)irq_ts_dev_irq_handler, // The pointer to the handler function below
                          irq_flag,                              // Interrupt on rising edge (button press, not release)
                          IRQ_TS_MODULE_NAME,                    // Used in /proc/interrupts to identify the owner
                          config_entryPtr);                      // The *dev_id for shared interrupt lines, NULL is okay
    if (ret_val != 0)
    {
        pr_err("irq_ts_dev_add_pin_monitor: request_irq failed: %d\n", ret_val);
        goto fail_free_irq;
    }
    pr_debug("irq_ts_dev_add_pin_monitor: request_irq success\n");

    config_entryPtr->enabled     = true;
    config_entryPtr->rising_edge = gpio_add_entry->rising_edge;
    config_entryPtr->gpio_pin    = gpio_add_entry->gpio_pin;
    config_entryPtr->polled      = 0;
    config_entryPtr->irq_num     = irq_num;
    strncpy(config_entryPtr->name, gpio_add_entry->name, IRQ_TS_IRQ_NAME_MAX_LEN);
    pr_debug("irq_ts_dev_add_pin_monitor: Successfully setup pin: %s\n", config_entryPtr->name);
    return 0;

fail_free_irq:
    pr_debug("irq_ts_dev_add_pin_monitor:  free_irq: %d\n", irq_num);
    free_irq(irq_num, config_entryPtr);
fail_unexport:
    pr_debug("irq_ts_dev_add_pin_monitor:  gpio_unexport: %d\n", gpio_add_entry->gpio_pin);
    gpio_unexport(gpio_add_entry->gpio_pin);
fail_remove_pin:
    pr_debug("irq_ts_dev_add_pin_monitor:  gpio_free: %d\n", gpio_add_entry->gpio_pin);
    gpio_free(gpio_add_entry->gpio_pin);
fail_exit:
    return ret_val;
}

static void irq_ts_dev_remove_pin_monitor(irq_ts_dev_pin_config_entry_t *entryPtr)
{
    pr_debug("irq_ts_dev_remove_pin_monitor:  free_irq:%d...\n", entryPtr->irq_num);
    free_irq(entryPtr->irq_num, entryPtr);

    pr_debug("irq_ts_dev_remove_pin_monitor:  gpio_unexport: %d...\n", entryPtr->gpio_pin);
    gpio_unexport(entryPtr->gpio_pin);

    pr_debug("irq_ts_dev_remove_pin_monitor:  gpio_free: %d...\n", entryPtr->gpio_pin);
    gpio_free(entryPtr->gpio_pin);

    pr_debug("irq_ts_dev_remove_pin_monitor:  clean entry...\n");
    memset((void *)(entryPtr), 0x00, sizeof(irq_ts_dev_pin_config_entry_t));
}

static long irq_ts_dev_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param_user)
{
    int ret = 0;
    int i;
    irq_ts_ioctl_param_union_t local_param;

    if (!mutex_trylock(&irq_ts_dev.ioctl_mutex))
    {
        return -EBUSY;
    }

    switch (ioctl_num)
    {

    case IRQ_TS_IOCTL_ADD_PIN:
    {
        pr_debug("IRQ_TS_IOCTL_ADD_PIN");
        // copy data from userspace
        if (copy_from_user((void *)&local_param, (void *)ioctl_param_user, _IOC_SIZE(ioctl_num)))
        {
            ret = -ENOMEM;
            pr_err("IRQ_TS_IOCTL_ADD_PIN unable to copy data from user space\n");
            break;
        }

        // use i as temporary variable
        i = local_param.add.entry_id;
        if (irq_ts_dev.pins[i].enabled)
        {
            if (irq_ts_dev.pins[i].gpio_pin == local_param.add.gpio_pin && irq_ts_dev.pins[i].rising_edge == local_param.add.rising_edge &&
                strcmp(irq_ts_dev.pins[i].name, local_param.add.name) == 0)
            {
                // identical entry, we done here
                pr_warn("Entry: %d is already set with the same data - ignoring\n", i);
            }
            else
            {
                pr_err("Entry: %d is already occupied; releaseing first\n", i);
                ret = -EINVAL;
            }
            break;
        }
        // try setup interrupt=
        ret = irq_ts_dev_add_pin_monitor(&local_param.add, &irq_ts_dev.pins[local_param.add.entry_id]);

        break;
    }
    case IRQ_TS_IOCTL_SET_TIMEOUT:
    {
        pr_debug("IRQ_TS_IOCTL_SET_TIMEOUT");
        // copy data from userspace
        if (copy_from_user((void *)&local_param, (void *)ioctl_param_user, _IOC_SIZE(ioctl_num)))
        {
            ret = -ENOMEM;
            pr_err("IRQ_TS_IOCTL_SET_TIMEOUT unable to copy data from user space\n");
            break;
        }

        // set read timeout
        irq_ts_dev.read_timeout_jiffies = msecs_to_jiffies(local_param.set_timeout.timeout_milisec);
        break;
    }
    case IRQ_TS_IOCTL_REMOVE_ENTRY:
    {
        pr_debug("IRQ_TS_IOCTL_REMOVE_ENTRY");
        // copy data from userspace
        if (copy_from_user((void *)&local_param, (void *)ioctl_param_user, _IOC_SIZE(ioctl_num)))
        {
            ret = -ENOMEM;
            pr_err("IRQ_TS_IOCTL_REMOVE_ENTRY unable to copy data from user space");
            break;
        }

        // check entry_id in within limits
        if (local_param.remove.entry_id >= IRQ_TS_PIN_COUNT)
        {
            ret = -EINVAL;
            pr_err("IRQ_TS_IOCTL_REMOVE_ENTRY Invalid entry_id");
            break;
        }

        // check if it is not enabled
        if (!irq_ts_dev.pins[local_param.remove.entry_id].enabled)
        {
            ret = -EINVAL;
            pr_err("IRQ_TS_IOCTL_REMOVE_ENTRY entry_id already disabled");
            break;
        }
        // try setup interrupt
        irq_ts_dev_remove_pin_monitor(&(irq_ts_dev.pins[local_param.remove.entry_id]));
        break;
    }
    case IRQ_TS_IOCTL_REPORT:
    {
        for (i = 0; i < IRQ_TS_PIN_COUNT; ++i)
        {
            local_param.report[i].entry_id    = i;
            local_param.report[i].rising_edge = irq_ts_dev.pins[i].rising_edge;
            local_param.report[i].enabled     = irq_ts_dev.pins[i].enabled;
            local_param.report[i].gpio_pin    = irq_ts_dev.pins[i].gpio_pin;
            if (irq_ts_dev.pins[i].enabled)
            {
                local_param.report[i].gpio_value = gpio_get_value(irq_ts_dev.pins[i].gpio_pin);
            }
            strncpy(local_param.report[i].name, irq_ts_dev.pins[i].name, IRQ_TS_IRQ_NAME_MAX_LEN);
        }
        if (copy_to_user((void *)ioctl_param_user, (void *)&local_param, _IOC_SIZE(ioctl_num)))
        {
            ret = -ENOMEM;
            pr_err("IRQ_TS_IOCTL_REPORT unable copy data to user space");
        }
        break;
    }

    default:
    {
        pr_err("irq_ts_dev_ioctl: no such command\n");
        ret = -EINVAL;
    }
    } /* end of switch(ioctl_num) */

    pr_debug("irq_ts_dev_ioctl DONE");
    mutex_unlock(&irq_ts_dev.ioctl_mutex);

    return ret;
}

static ssize_t irq_ts_dev_read(struct file *filp, char __user *buf, size_t len, loff_t *loffset)
{
    irq_ts_read_t read_data;
    int i;
    size_t data_len;
    irq_ts_dev_pin_config_entry_t *pinEntPtr;
    unsigned long s;

#ifdef IRQ_TS_BLOCKING_CALL
    if (filp->f_flags & O_NONBLOCK)
    {
        return -EAGAIN;
    }
#endif
    // we do not support reading partial data
    if (*loffset != 0)
    {
        pr_err("irq_ts_dev_read: offset is not supported\n");
        return -EINVAL;
    }

    // make sure the length is sufficient
    data_len = sizeof(irq_ts_read_t);
    if (data_len > len)
    {
        pr_err("irq_ts_dev_read: buffer too short\n");
        return -EFBIG;
    }

#ifdef IRQ_TS_BLOCKING_CALL
    i = wait_event_interruptible_timeout(irq_ts_dev.read_blocking_queue, irq_ts_dev.have_new_data == true, irq_ts_dev.read_timeout_jiffies);

    // interrupted by signal -ERESTARTSYS
    if (i == -ERESTARTSYS)
    {
        /* signal interrupted the wait, return */
        pr_err("irq_ts_dev_read: read interrupted by signal");
        return i;
    }
    // 0 - timeout with condition not met
    else if (i == 0)
    {
        /* signal interrupted the wait, return */
        // pr_debug("irq_ts_dev_read: read timeout");
        return i;
    }
#endif
    spin_lock_irqsave(&irq_ts_dev.spin_irq_lock, s);
    pinEntPtr = irq_ts_dev.pins;
    for (i = 0; i < IRQ_TS_PIN_COUNT; ++i)
    {
        read_data[i].entry_id = i;
        read_data[i].seq      = pinEntPtr->seq;

        // if the ts is 0 it means we already sent it or it was never set
        if (pinEntPtr->last_ts.tv_sec != 0)
        {
            read_data[i].has_time = true;
            read_data[i].sec      = pinEntPtr->last_ts.tv_sec;
            read_data[i].nsec     = pinEntPtr->last_ts.tv_nsec;
#ifdef IRQ_TS_BLOCKING_CALL
            // set the timestamp to zero so we will not send it again
            // this is only done for blocking call for non blocking we just return latest
            pinEntPtr->last_ts.tv_sec  = 0;
            pinEntPtr->last_ts.tv_nsec = 0;
#endif
        }
        else
        {
            read_data[i].has_time = false;
            read_data[i].sec      = 0;
            read_data[i].nsec     = 0;
        }

        pinEntPtr++;
    }
    irq_ts_dev.have_new_data = false;
    spin_unlock_irqrestore(&irq_ts_dev.spin_irq_lock, s);
    if (copy_to_user(buf, &read_data, data_len))
    {
        return -ENOMEM;
    }
    return data_len;
}

// TODO: fix all functions to ststic
// TODO: fix functions names to irq_ts_dev_...
static int irq_ts_dev_open(struct inode *inode, struct file *file)
{
    pr_debug("irq_ts_dev_open");
    return 0;
}

static int irq_ts_dev_close(struct inode *inodep, struct file *filp)
{
    pr_debug("irq_ts_dev_close");
    return 0;
}

// clang-format off
static const struct file_operations irq_ts_dev_fops = {
    .owner = THIS_MODULE,
    .read = irq_ts_dev_read,
    .unlocked_ioctl = irq_ts_dev_ioctl,
    .open = irq_ts_dev_open,
    .release = irq_ts_dev_close,
    .llseek = no_llseek,
};

struct miscdevice irq_ts_dev_device =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = IRQ_TS_MODULE_NAME,
    .fops = &irq_ts_dev_fops,
    .mode = S_IRUGO | S_IWUGO
};
// clang-format on

static int __init irq_ts_dev_init(void)
{
    int error;
    mutex_init(&irq_ts_dev.ioctl_mutex);
    spin_lock_init(&irq_ts_dev.spin_irq_lock);
#ifdef IRQ_TS_BLOCKING_CALL
    init_waitqueue_head(&irq_ts_dev.read_blocking_queue);
#endif
    DEBUG_SETUP_PIN();
    memset((void *)irq_ts_dev.pins, 0x00, sizeof(irq_ts_dev.pins));
    irq_ts_dev.have_new_data        = false;
    irq_ts_dev.read_timeout_jiffies = msecs_to_jiffies(IRQ_TS_IRQ_READ_TIMEOUT_DEFAULT_MSEC);

    pr_info("irq_ts_init\n");

    error = misc_register(&irq_ts_dev_device);
    if (error)
    {
        pr_err("irq_ts_init misc_register failed(\n");
        return error;
    }

    return 0;
}

static void __exit irq_ts_dev_exit(void)
{
    int i;
    pr_info("removing device misc_deregister\n");
    for (i = 0; i < IRQ_TS_PIN_COUNT; ++i)
    {
        if (irq_ts_dev.pins[i].enabled)
        {
            pr_warn("Freeing unclean pin entry: %s (entry_id: %d)\n", irq_ts_dev.pins[i].name, i);
            irq_ts_dev_remove_pin_monitor(&(irq_ts_dev.pins[i]));
            irq_ts_dev.pins[i].enabled = false;
        }
    }
    misc_deregister(&irq_ts_dev_device);
    DEBUG_RELEASE_PIN();
    pr_debug("irq_ts_exit\n");
}

module_init(irq_ts_dev_init);
module_exit(irq_ts_dev_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Kernel Module that captures GPIO timestamps");
MODULE_AUTHOR("Maciej Matuszak");
