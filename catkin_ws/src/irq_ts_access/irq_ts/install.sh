#!/bin/bash

MODULENAME=irq_ts

if [[ $EUID -ne 0 ]] ;
then
   echo "This install script must be run as root" 1>&2
   exit 1
fi

echo installing udev rules
sudo cp -f 99-irq_ts_udev.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules
#check if module installed
lsmod | grep $MODULENAME > /dev/null
if [ $? -eq 0 ]
then
  echo "removing old module..."
  sudo rmmod $MODULENAME
fi

echo "installing module..."
sudo insmod $MODULENAME.ko
