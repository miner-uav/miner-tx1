#ifndef IRQTSACCESS_H
#define IRQTSACCESS_H

#include <linux/irq_ts.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <boost/make_shared.hpp>

namespace irq_ts_access
{

using namespace std;

class IrqTsAccess
{
  public:
    typedef struct IrqTsAccessTimestamp_s
    {
        unsigned int entry_id;
        unsigned int seq;
        bool has_time;
        long sec;
        long nsec;
        long us_sec;
        long us_usec;

    } IrqTsAccessTimestamp_t;

    IrqTsAccess(const string devName);
    void Open();
    void OpenRw();
    int Close();
    int Poll();
    int Read(std::vector<boost::shared_ptr<IrqTsAccessTimestamp_t>> &data);
    bool Read(uint32_t entry_id, long *seconds, long *nano_seconds, uint32_t *seq);
    int Report(irq_ts_ioctl_report_t *reportPtr);
    void AddPin(uint8_t entruID, uint32_t gpioPin, bool risingEdge, std::string name);

protected:
    string mDeviceName;
    int mFD;
};
}
#endif // IRQTSACCESS_H
