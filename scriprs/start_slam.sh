#!/usr/bin/env bash


. /opt/ros/kinetic/setup.bash
. /data/git/miner-tx1/catkin_ws/devel/setup.bash

rosrun ros_orb_slam2 ros_orb_slam2_stereo_node /data/git/miner-tx1/catkin_ws/src/orb_slam_gpu/Vocabulary/ORBvoc.txt  /data/git/miner-tx1/catkin_ws/src/orb_slam_gpu/cfg/orb_slam_zed.yaml false \
/camera/left/image_raw:=/zed/left/image_rect_color \
/camera/right/image_raw:=/zed/right/image_rect_color
