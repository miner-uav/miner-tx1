#!/usr/bin/env bash


. /opt/ros/kinetic/setup.bash
. /data/git/miner-tx1/catkin_ws/devel/setup.bash

roslaunch zed_wrapper zed.launch resolution:=3 frame_rate:=30
