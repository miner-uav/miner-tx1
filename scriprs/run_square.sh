#!/usr/bin/env bash

read -p "Press any key to go to (0, 0) @height 2m" -n1 -s
rostopic pub -1 /miner/command/trajectory trajectory_msgs/MultiDOFJointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- ''
points:
- transforms:
  - translation:
      x: 0.0
      y: 0.0
      z: 2.0
" 
read -p "Press any key to go to forward 1m" -n1 -s

rostopic pub -1 /miner/command/trajectory trajectory_msgs/MultiDOFJointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- ''
points:
- transforms:
  - translation:
      x: 1.0
      y: 0.0
      z: 2.0
" 
read -p "Press any key to go to left 1m" -n1 -s

rostopic pub -1 /miner/command/trajectory trajectory_msgs/MultiDOFJointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- ''
points:
- transforms:
  - translation:
      x: 1.0
      y: 1.0
      z: 2.0
" 

read -p "Press any key to go to back 1m" -n1 -s

rostopic pub -1 /miner/command/trajectory trajectory_msgs/MultiDOFJointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- ''
points:
- transforms:
  - translation:
      x: 0.0
      y: 1.0
      z: 2.0
" 
read -p "Press any key to go to right 1m" -n1 -s

rostopic pub -1 /miner/command/trajectory trajectory_msgs/MultiDOFJointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- ''
points:
- transforms:
  - translation:
      x: 0.0
      y: 0.0
      z: 2.0
" 
